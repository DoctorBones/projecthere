package hibernateroutes;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name="solved_problem")
public class HSolvedProblem implements Serializable {

    @Id
    @OneToOne
    @JoinColumn(name="problem_id",referencedColumnName = "problem_id")
    private HProblem problemId;

    @Column(name="weight")
    private Integer weight;

    public HProblem getProblemId() {
        return problemId;
    }

    public void setProblemId(HProblem problemId) {
        this.problemId = problemId;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(Integer weight) {
        this.weight = weight;
    }

    public HSolvedProblem(HProblem problemId, Integer weight) {
        this.problemId = problemId;
        this.weight = weight;
    }

    public HSolvedProblem() {
    }
}
