package hibernateroutes;

import javax.persistence.*;

@Entity
@Table(name="problem")
public class HProblem {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "problem_id")
    private Integer problemId;

    @ManyToOne
    @JoinColumn(name="from_city_id")
    private HCity from;


    @ManyToOne
    @JoinColumn(name="to_city_id")
    private HCity to;

    public HProblem(HCity from, HCity to) {
        this.from = from;
        this.to = to;
    }

    public HProblem() {
    }

    public Integer getProblemId() {
        return problemId;
    }

    public void setProblemId(Integer problemId) {
        this.problemId = problemId;
    }

    public HCity getFrom() {
        return from;
    }

    public void setFrom(HCity from) {
        this.from = from;
    }

    public HCity getTo() {
        return to;
    }

    public void setTo(HCity to) {
        this.to = to;
    }
}
