package hibernateroutes;


import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashSet;
import java.util.List;
import java.util.Set;


public class UploadHData {
    private static final Set loadClasses= new HashSet(List.of(HCity.class,HEdge.class,
            HProblem.class,HSolvedProblem.class,HUnsolvableProblem.class));
    private static final Logger logger= LoggerFactory.getLogger(UploadHData.class);
    public void load(Object obj) throws Exception{
        if(!(loadClasses.contains(obj.getClass()))){
            logger.info("Tried to upload incompatible object");
        }
        try(Session session=HibernateUtil.getSessionFactory().openSession()){
            try{
               session.beginTransaction();
               session.save(obj);
               session.getTransaction().commit();
               logger.info("Uploaded successfully "+obj.getClass());
            }
            catch (Exception e){
                session.getTransaction().rollback();
                throw e;
            }
        }
    }
    public static void main(String[] args) {
        HCity hCity1 = new HCity();
        hCity1.setId(1);
        hCity1.setName("Gdansk");
        HCity hCity2 = new HCity();
        hCity2.setId(2);
        hCity2.setName("bydgos");
        HCity hCity3 = new HCity();
        hCity3.setId(3);
        hCity3.setName("torun");
        HCity hCity4 = new HCity();
        hCity4.setId(4);
        hCity4.setName("warszawa");
        hCity1.setFromEdges(List.of(new HEdge(hCity1,hCity2,1),new HEdge(hCity1,hCity3,3)));
        hCity2.setFromEdges(List.of(new HEdge(hCity2,hCity1,1),new HEdge(hCity2,hCity3,1),new HEdge(hCity2,hCity4,4)));
        hCity3.setFromEdges(List.of(new HEdge(hCity3,hCity1,3),new HEdge(hCity3,hCity2,1),new HEdge(hCity3,hCity4,1)));
        hCity4.setFromEdges(List.of(new HEdge(hCity4,hCity2,4),new HEdge(hCity4,hCity3,1)));
        HProblem hproblem = new HProblem(hCity1, hCity4);
        HProblem hproblem1 = new HProblem(hCity2, hCity4);
        UploadHData loader=new UploadHData();
        try {
            loader.load(hCity1);
            loader.load(hCity2);
            loader.load(hCity3);
            loader.load(hCity4);
            loader.load(hproblem);
            loader.load(hproblem1);
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }
}
