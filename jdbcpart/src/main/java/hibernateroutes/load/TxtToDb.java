package hibernateroutes.load;

import hibernateroutes.HCity;
import hibernateroutes.HEdge;
import hibernateroutes.HProblem;
import hibernateroutes.HibernateUtil;
import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class TxtToDb {
    Map<Integer, HCity> cities;
    List<HProblem> problems;
    private static final Logger logger= LoggerFactory.getLogger(TxtToDb.class);

    public TxtToDb() {
        cities=new HashMap<>();
        problems=new ArrayList<>();
    }

    public void createCities(String filename)throws IOException {
        try(BufferedReader reader=new BufferedReader(new FileReader(filename))){
            int n=Integer.parseInt(reader.readLine());
            Map<String, Integer> map=new HashMap<>();
            for(int i=1;i<=n;i++){
                HCity city=new HCity();
                String name=reader.readLine();
                city.setName(name);
                city.setId(i);
                var list=new LinkedList<HEdge>();
                int routesNumber=Integer.parseInt(reader.readLine());
                for(int j=0;j<routesNumber;j++){
                    String[] edge=reader.readLine().split(" ");
                    int key=Integer.parseInt(edge[0]);
                    if(!(cities.containsKey(Integer.parseInt(edge[0])))) {
                        HCity toCity=new HCity();
                        toCity.setId(key);
                        cities.put(key, toCity);
                    }
                    logger.info("Edge from " + (i) +" "+cities.get(key).getId());
                    list.add(new HEdge(city, cities.get(key),Integer.parseInt(edge[1])));
                }
                logger.info("City: "+city.getName() + " was created with number of edges: "+list.size());
                if(cities.containsKey(i)) {
                    cities.get(i).setName(name);
                    cities.get(i).setFromEdges(list);
                }
                else{
                    city.setFromEdges(list);
                    cities.put(i,city);
                }
                map.put(name,i);
            }
            int numberTofind=Integer.parseInt(reader.readLine());
            for(int i=0;i<numberTofind;i++){
                String [] route=reader.readLine().split(" ");
                problems.add(new HProblem(cities.get(map.get(route[0])), cities.get(map.get(route[1]))));
                logger.info("Problem: from-"+route[0]+" to-"+route[1]);
            }
        }
    }

    public void loadToDb() throws Exception{
        try(Session session= HibernateUtil.getSessionFactory().openSession()){
            try{
                session.beginTransaction();
                for(Object key:cities.keySet()){
                    session.save(cities.get(key));
                    logger.info("City was loaded to db ");
                }
                for(int i=0, n=problems.size();i<n;i++){
                    session.save(problems.get(i));
                    logger.info("Task was loaded to db ");
                }
                session.getTransaction().commit();
            }
            catch (Exception e){
                session.getTransaction().rollback();
                throw e;
            }
        }
    }

    public static void main(String[] args) {
        try {
            TxtToDb txtToDb = new TxtToDb();
            txtToDb.createCities("jdbcpart/src/main/resources/routes.txt");
            txtToDb.loadToDb();
        }
        catch (Exception e){
            logger.info("Exception has ocurred" +e.getClass());
            e.printStackTrace();
        }
    }
}
