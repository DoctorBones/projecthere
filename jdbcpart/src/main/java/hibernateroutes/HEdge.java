package hibernateroutes;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name="edge")
public class HEdge implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "edge_id")
    private Integer edgeId;


    @ManyToOne
    @JoinColumn(name="from_city_id")
    private HCity fromCity;


    @ManyToOne
    @JoinColumn(name="to_city_id")
    private HCity toCity;


    @Column(name="weight")
    private Integer weight;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        HEdge hEdge = (HEdge) o;
        return Objects.equals(fromCity, hEdge.fromCity) &&
                Objects.equals(toCity, hEdge.toCity) &&
                Objects.equals(weight, hEdge.weight);
    }

    @Override
    public int hashCode() {
        return Objects.hash(fromCity, toCity, weight);
    }

    public HEdge(HCity fromCity, HCity toCity, Integer weight) {
        this.fromCity = fromCity;
        this.toCity = toCity;
        this.weight = weight;
    }
    public HEdge() {
    }

    public HCity getFromCity() {
        return fromCity;
    }

    public void setFromCity(HCity fromCity) {
        this.fromCity = fromCity;
    }

    public HCity getToCity() {
        return toCity;
    }

    public void setToCity(HCity toCity) {
        this.toCity = toCity;
    }

    public Integer getWeight() {
        return weight;
    }

    public void setWeight(Integer weight) {
        this.weight = weight;
    }
    public Integer getEdgeId() {
        return edgeId;
    }

    public void setEdgeId(Integer edgeId) {
        this.edgeId = edgeId;
    }


}
