package hibernateroutes;

import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class HibernateUtil {
    private static final SessionFactory sessionFactory=configureSessionFactory();

    private static SessionFactory configureSessionFactory() throws HibernateException {
        Configuration cfg = new Configuration().configure();
        return cfg.buildSessionFactory();
    }
    public static SessionFactory getSessionFactory(){
        return sessionFactory;
    }
}
