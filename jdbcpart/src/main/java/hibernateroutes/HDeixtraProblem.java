package hibernateroutes;

import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;


public class HDeixtraProblem {
    private static final Logger logger= LoggerFactory.getLogger(HDeixtraProblem.class);
    private Map<Integer, HCity> cities;
    public static List<HProblem> findUnsolvedProblems(EntityManager entityManager){
        Query query=entityManager.createNativeQuery("select * from problem where problem_id " +
                "not in (select problem_id from solved_problem) " +
                        "and problem_id not in (select problem_id from unsolvable_problem)",HProblem.class);
        logger.info("Problems were loaded");
        return query.getResultList();
        /*select * from problem where problem_id " +
        "not in (select problem_id from solved_problem) " +
                "and problem_id not in (select problem_id from unsolvable_problem);*/

    }
    public static List<HCity> loadCities(EntityManager entityManager){
        Query query=entityManager.createQuery("from HCity city",HCity.class);
        logger.info("Cities were loaded");

        return query.getResultList();
    }
    private void refresh() {
        var keys=cities.keySet();
        var iter=keys.iterator();
        while(iter.hasNext())
            cities.get(iter.next()).setBestWay(Integer.MAX_VALUE);
    }

    public int cheapestWay(HCity begin, HCity end) {
        refresh();
        HCity temp = begin;
        temp.setBestWay(0);
        var visited=new HashSet<HCity>();
        var keys=cities.keySet();
        for(Integer index:keys){
            visited.add(cities.get(index));
        }
        while (temp.getName() != end.getName()) {
            var list = temp.getFromEdges();
            for (int i = 0, n = list.size(); i < n; i++) {
                HCity city = cities.get(list.get(i).getToCity().getId());
                if (!(visited.contains(city))) {
                    continue;
                }
                if(temp.getBestWay()+list.get(i).getWeight()<city.getBestWay()) {
                    city.setBestWay(temp.getBestWay()+list.get(i).getWeight());
                }
            }
            visited.remove(temp);
            temp = visited.stream().min(Comparator.comparingInt(c->c.getBestWay())).
                    orElse(new HCity());

        }
        return temp.getBestWay();
    }

    public void findSolutions() {
        try(Session session=HibernateUtil.getSessionFactory().openSession()) {
            cities = loadCities(session).stream().collect(Collectors.toMap(HCity::getId, Function.identity()));
            List<HProblem> hproblem = findUnsolvedProblems(session);
            if(hproblem.size()==0){
                logger.info("No tasks to solve(");
                return;
            }
            try {
                session.beginTransaction();
                System.out.println(cities.get(1).getName());
                for (int i = 0; i < hproblem.size(); i++) {
                    HProblem problem = hproblem.get(i);
                    logger.info("Problem: " + problem.getProblemId() + " " + problem.getFrom() + " " + problem.getTo());
                    int way = cheapestWay(problem.getFrom(), problem.getTo());
                    logger.info("Found solution to problem with id " + problem.getProblemId() + ". Way: " + way);
                    if(way==Integer.MAX_VALUE){
                        HUnsolvableProblem unp=new HUnsolvableProblem(problem);
                        session.save(unp);
                    }
                    else{
                        HSolvedProblem unp=new HSolvedProblem(problem,way);
                        session.save(unp);
                    }

                }
                session.getTransaction().commit();
            }
            catch (Exception e){
                session.getTransaction().rollback();
                throw e;
            }
        }
    }

    public static void main(String[] args) {
        HDeixtraProblem solve=new HDeixtraProblem();
        solve.findSolutions();
        /*try(Session session=HibernateUtil.getSessionFactory().openSession()) {
            EntityManager entityManager=HibernateUtil.getSessionFactory().createEntityManager();
            Query query=entityManager.createQuery("from HEdge edge", HEdge.class);
        }*/
    }
}
