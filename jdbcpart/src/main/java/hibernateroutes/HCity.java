package hibernateroutes;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name="city")
public class HCity {
    @Id
    @Column(name = "city_id", nullable = false)
     private Integer id;

    @Column(name="name",nullable = false)
    private String name;

    @Transient
    private Integer bestWay;

    @OneToMany(mappedBy = "fromCity",cascade = CascadeType.ALL, orphanRemoval = true)
    private List<HEdge> fromEdges;

    @OneToMany(mappedBy = "toCity", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<HEdge> toEdges;

    public HCity() {

    }

    public HCity(Integer id, String name, List<HEdge> fromEdges, List<HEdge> toEdges) {
        this.id=id;
        this.name = name;
        this.fromEdges = fromEdges;
        this.toEdges = toEdges;
    }
    public void addEdge(HEdge edge){
        if(this.equals(edge.getFromCity().id)){
            return;
        }
        edge.setFromCity(this);
        fromEdges.add(edge);
        edge.getToCity().toEdges.add(edge);

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getBestWay() {
        return bestWay;
    }

    public void setBestWay(Integer bestWay) {
        this.bestWay = bestWay;
    }

    public List<HEdge> getFromEdges() {
        return fromEdges;
    }

    public List<HEdge> getToEdges() {
        return toEdges;
    }

    public void setToEdges(List<HEdge> toEdges) {
        this.toEdges = toEdges;
    }

    public void setFromEdges(List<HEdge> fromEdges) {
        this.fromEdges = fromEdges;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        HCity hCity = (HCity) o;
        return Objects.equals(id, hCity.id) &&
                Objects.equals(name, hCity.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name);
    }
}
