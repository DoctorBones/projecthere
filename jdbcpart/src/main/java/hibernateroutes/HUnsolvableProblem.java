package hibernateroutes;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name="unsolvable_problem")
public class HUnsolvableProblem implements Serializable {
    @Id
    @OneToOne
    @JoinColumn(name="problem_id")
    private HProblem problemId;

    public HProblem getProblemId() {
        return problemId;
    }

    public void setProblemId(HProblem problemId) {
        this.problemId = problemId;
    }

    public HUnsolvableProblem(HProblem problemId) {
        this.problemId = problemId;
    }

    public HUnsolvableProblem() {
    }
}
