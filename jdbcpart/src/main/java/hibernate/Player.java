package hibernate;


import javax.persistence.*;

@Entity
@Table(name="players")
public class Player {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "player_id")
    private long id;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "score")
    private Integer score;

    @ManyToOne
    @JoinColumn(name="guild_id")
    private Guild guild;

    public Player(String name, Integer score, Guild guild) {
        this.name = name;
        this.score = score;
        this.guild = guild;
    }

    public Player() {
    }
}
