package hibernate;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name="guild")
public class Guild {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "guild_id")
    private long id;



    @Column(name="name")
    private String name;

    @OneToMany(mappedBy = "guild")
    private List<Player> players;

    public Guild() {
    }

    public Guild(String name, List<Player> players) {
        this.name = name;
        this.players = players;
    }
}
