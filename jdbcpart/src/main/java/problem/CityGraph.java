package problem;

import java.util.*;
import java.util.stream.Collectors;

public class CityGraph {
    static class City {
        String name;
        int bestWay;
        LinkedList<Edge> edges;

        public void setBestWay(int bestWay) {
            if (bestWay < this.bestWay) {
                this.bestWay = bestWay;
            }
        }

        public void setStartCheapesWay() {
            this.bestWay = Integer.MAX_VALUE;
        }

        public City(String name, LinkedList<Edge> edges) {
            this.name = name;
            this.edges = edges;

        }

        public String toString() {
            return name + "\n" + edges.toString() + "BW: " + bestWay;
        }
    }

    static class Edge {
        int to;
        int weight;

        public Edge(int to, int weight) {
            this.to = to;
            this.weight = weight;
        }

        public String toString() {
            return to + "-" + weight;
        }

    }

    private HashMap<Integer,City> cities;

    public CityGraph(HashMap<Integer,City> cities) {
        this.cities = cities;
    }

    private void refresh() {
        var keys=cities.keySet();
        var iter=keys.iterator();
        while(iter.hasNext())
            cities.get(iter.next()).setStartCheapesWay();

    }


    public HashMap<Integer,City> getCities() {
        return cities;
    }


    public int cheapestWay(City begin, City end) {
        refresh();
        City temp = begin;
        temp.setBestWay(0);
        var visited=new HashSet<City>();
        var keys=cities.keySet();
        for(Integer index:keys){
            visited.add(cities.get(index));
        }
        while (temp.name != end.name) {
            var list = temp.edges;
            for (int i = 0, n = list.size(); i < n; i++) {
                City city = cities.get(list.get(i).to);
                if (!(visited.contains(city))) {
                    continue;
                }
                city.setBestWay(temp.bestWay + list.get(i).weight);
            }
            visited.remove(temp);
            temp = visited.stream().min(Comparator.comparingInt(c->c.bestWay)).orElse(new City("NoCities",new LinkedList<>()));

        }
        return temp.bestWay;
    }

    public static void main(String[] args) {
        CityGraph cityGraph;
        LinkedList<CityGraph.Edge> list=new LinkedList<>();
        list.add(new CityGraph.Edge(2,1));
        list.add(new CityGraph.Edge(3,3));
        var cities=new HashMap<Integer,City>();
        cities.put(1,new CityGraph.City("gdansk",list));
        list=new LinkedList<CityGraph.Edge>();
        list.add(new CityGraph.Edge(1,1));
        list.add(new CityGraph.Edge(3,1));
        list.add(new CityGraph.Edge(4,4));
        cities.put(2,new CityGraph.City("bydgo",list));
        list=new LinkedList<CityGraph.Edge>();
        list.add(new CityGraph.Edge(1,3));
        list.add(new CityGraph.Edge(2,1));
        list.add(new CityGraph.Edge(4,1));
        cities.put(3,new CityGraph.City("torun",list));
        list=new LinkedList<CityGraph.Edge>();
        list.add(new CityGraph.Edge(2,4));
        list.add(new CityGraph.Edge(3,1));
        cities.put(4,new CityGraph.City("Warszawa",list));
        cityGraph=new CityGraph(cities);
        System.out.println(cityGraph.cheapestWay(cities.get(1),cities.get(4)));
        System.out.println(cityGraph.cheapestWay(cities.get(2),cities.get(4)));
    }
}
