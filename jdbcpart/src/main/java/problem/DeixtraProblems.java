package problem;


import java.util.ArrayList;

public class DeixtraProblems {
    public static class Problem{
        int problemId;
        int from;
        int to;

        public Problem(int problemId, int from, int to) {
            this.problemId = problemId;
            this.from = from;
            this.to = to;
        }
        public boolean equals(Object obj){
            if(this==obj){
                return true;
            }
            if(obj==null||obj.getClass()!=this.getClass()){
                return false;
            }
            Problem problem=(Problem) obj;
            return problem.problemId==this.problemId && problem.from==this.from && problem.to==this.to;
        }
        public String toString(){
            return "ID: "+problemId+". From_id: "+from+". To_id: "+to;
        }
    }
    ArrayList<Problem> problems;

    public DeixtraProblems(ArrayList<Problem> problems) {
        this.problems = problems;
    }
}
