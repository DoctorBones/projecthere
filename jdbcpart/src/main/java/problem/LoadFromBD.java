package problem;

import java.io.IOException;
import java.sql.*;
import java.util.*;

public class LoadFromBD {
    private final Properties prop;
    private final String url;

    public LoadFromBD() throws IOException, SQLException {
        this.prop = loadProperties();
        this.url = prop.getProperty("url");
    }

    public static Properties loadProperties() throws IOException {
        var prop = new Properties();

        try (var input = LoadFromBD.class.getClassLoader().getResourceAsStream("jdbc.properties")) {
            prop.load(input);
        } catch (IOException e) {
            throw new IOException();
        }
        return prop;
    }

    public DeixtraProblems problem(String sql) throws IOException, SQLException {
        var problems = new ArrayList<DeixtraProblems.Problem>();
        try {
            try (var connection = DriverManager.getConnection(url, prop)) {
                try (PreparedStatement stm = connection.prepareStatement(sql)) {
                    ResultSet set = stm.executeQuery();
                    while(set.next()){
                        DeixtraProblems.Problem toAdd =
                                new DeixtraProblems.Problem(set.getInt(1),
                                set.getInt(2), set.getInt(3));
                        problems.add(toAdd);
                    }
                }

            }
        } catch (SQLException e) {
            throw e;
        }
        return new DeixtraProblems(problems);
    }

    public LinkedList<CityGraph.Edge> loadEdge(int city_to, String sql) throws SQLException{
        var list=new LinkedList<CityGraph.Edge>();
        try {
            try (var connection = DriverManager.getConnection(url, prop)) {
                try (PreparedStatement stm = connection.prepareStatement(sql)) {
                    stm.setString(1, Integer.toString(city_to));
                    ResultSet set = stm.executeQuery();
                    while(set.next()){
                        var toAdd = new CityGraph.Edge(set.getInt(3),set.getInt(1));
                        list.add(toAdd);
                    }
                }
            }
        }
        catch (SQLException e){
            throw e;
        }
        return list;
    }
    public void insertIntoDB(String sql,int ... args) throws SQLException{
        try {
            try (var connection = DriverManager.getConnection(url, prop)) {
                try (PreparedStatement stm = connection.prepareStatement(sql)) {
                    for(int i=1, n=args.length;i<=n;i++){
                        stm.setString(i,Integer.toString(args[i-1]));
                    }
                    stm.execute();
                }
            }
        }
        catch (SQLException e){
            throw e;
        }
    }
    public CityGraph loadCities(int startCity, String sql) throws SQLException{
        var result=new HashMap<Integer,CityGraph.City>();
        HashSet<Integer> aboutToUpload=new HashSet<>();
        aboutToUpload.add(startCity);
        int city_id=startCity;
        try {
            try (var connection = DriverManager.getConnection(url, prop)) {
                while (aboutToUpload.size()!=0) {
                    try (PreparedStatement stm = connection.prepareStatement(sql)) {
                        stm.setString(1, Integer.toString(city_id));
                        ResultSet set = stm.executeQuery();
                        if(set.next()){

                            CityGraph.City city=new CityGraph.City(set.getString(2),
                                    loadEdge(city_id, "select * from connection where from_city=?;"));
                            result.put(city_id,city);
                            aboutToUpload.remove(city_id);
                            for(int i=0, n=city.edges.size();i<n;i++){
                                if(!(result.containsKey(city.edges.get(i).to))&&
                                        !(aboutToUpload.contains(city.edges.get(i).to))){
                                    aboutToUpload.add(city.edges.get(i).to);
                                }
                            }
                        }
                    }
                    if(!(aboutToUpload.isEmpty())) {
                        city_id = aboutToUpload.iterator().next();
                    }
                }
            }
        }
        catch (SQLException e){
            throw e;
        }
        return new CityGraph(result);
    }
    public void solveProblem()throws SQLException, IOException{
        try{
            DeixtraProblems tasks=problem(
                    "select * from problem where problem_id not in (select problem_id from found_routes) " +
                            "and problem_id not in (select problem_id from impossible_routes);");
            for(DeixtraProblems.Problem problem:tasks.problems){
                CityGraph cityGraph=loadCities(problem.from,"select * from city where city_id=?;");
                if(!(cityGraph.getCities().containsKey(problem.to))){
                    insertIntoDB("insert into impossible_routes() values(?);",problem.problemId );
                    continue;
                }
                int min_route=cityGraph.cheapestWay(cityGraph.getCities().get(problem.from),
                        cityGraph.getCities().get(problem.to));
                insertIntoDB("insert into found_routes() values(?,?);",problem.problemId,min_route);
            }
        }
        catch (SQLException e){
            throw e;
        }
        catch (IOException e){
            throw e;
        }
    }
    public static void main(String[] args) {

        try {
            LoadFromBD loadFromBD = new LoadFromBD();
            loadFromBD.solveProblem();

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
