package problem;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;

import static org.junit.jupiter.api.Assertions.*;

class LoadFromBDTest {
    LoadFromBD loadFromBD;

    @BeforeEach
    void setUp(){
        try {
            loadFromBD = new LoadFromBD();
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }
    @Test
    public void testingCityUpload(){
        try {
            CityGraph cityGraph = loadFromBD.loadCities(1, "select * from city where city_id=?;");
            CityGraph cityGraph1;
            LinkedList<CityGraph.Edge> list=new LinkedList<>();
            list.add(new CityGraph.Edge(2,1));
            list.add(new CityGraph.Edge(3,3));
            var cities=new HashMap<Integer, CityGraph.City>();
            cities.put(1,new CityGraph.City("gdansk",list));
            list=new LinkedList<CityGraph.Edge>();
            list.add(new CityGraph.Edge(1,1));
            list.add(new CityGraph.Edge(3,1));
            list.add(new CityGraph.Edge(4,4));
            cities.put(2,new CityGraph.City("bydgo",list));
            list=new LinkedList<CityGraph.Edge>();
            list.add(new CityGraph.Edge(1,3));
            list.add(new CityGraph.Edge(2,1));
            list.add(new CityGraph.Edge(4,1));
            cities.put(3,new CityGraph.City("torun",list));
            list=new LinkedList<CityGraph.Edge>();
            list.add(new CityGraph.Edge(2,4));
            list.add(new CityGraph.Edge(3,1));
            cities.put(4,new CityGraph.City("Warszawa",list));
            cityGraph1=new CityGraph(cities);
            assertEquals(cityGraph1.cheapestWay(cities.get(1),cities.get(4)),cityGraph.cheapestWay(cityGraph.getCities().get(1),
                    cityGraph.getCities().get(4)));

        }

        catch (Exception e){
            e.printStackTrace();
        }
    }
    @Test
    public void testingProblemUpload() {
        try {
            var problems = loadFromBD.problem("select * from problem where problem_id not in (select problem_id from found_routes) " +
                    "and problem_id not in (select problem_id from impossible_routes);");
            var createTasks=new ArrayList<DeixtraProblems.Problem>();
            createTasks.add(new DeixtraProblems.Problem(1,1,4));
            createTasks.add(new DeixtraProblems.Problem(2,2,4));
            DeixtraProblems problemsManual=new DeixtraProblems(createTasks);
            assertEquals(createTasks.get(0),problems.problems.get(0));
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

}