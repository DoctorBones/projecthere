import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.concurrent.ConcurrentHashMap;

@WebServlet(name = "count-servlet", urlPatterns = "/count")
public class IpCollect extends HttpServlet {
    private static final Logger logger= LoggerFactory.getLogger(IpCollect.class);
    private final ConcurrentHashMap<String,String> map=new ConcurrentHashMap<>();

    @Override
    public void init() {
        logger.info("Sample Servlet initialized");
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        PrintWriter responseBody = resp.getWriter();
        map.put(req.getRemoteAddr(),req.getHeader("User-Agent"));
        var set=map.keySet();
        for(Object key:set){
            responseBody.print("<p><b>"+key+" "+map.get(key)+"</b></p>");
        }


    }

    @Override
    public void destroy() {
        logger.info("Sample Servlet destroyed");
    }

}
