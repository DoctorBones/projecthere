package todolist.controller;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import todolist.entity.ToDo;
import todolist.entity.request.ToDoRequest;
import todolist.exception.ToDoNotFoundException;
import todolist.service.ToDoCRUD;

import java.util.List;

@RestController
@RequestMapping("/todo")
public class Controller {
    private final ToDoCRUD toDoCRUD;

    public Controller(ToDoCRUD toDoCRUD) {
        this.toDoCRUD = toDoCRUD;
    }

    @GetMapping("/{id}")
    public ToDo get(@PathVariable Long id){
        return toDoCRUD.getById(id).orElseThrow(()->new ToDoNotFoundException(id));
    }
    @GetMapping("/unfinished")
    public List<ToDo> getUnfinished(){
        return toDoCRUD.findUnfinished();
    }

    @GetMapping("/todolist")
    public List<ToDo> getAll(){
        return toDoCRUD.findAll();
    }

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping
    public ToDo create(@RequestBody ToDoRequest request){
        return toDoCRUD.create(request);
    }
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PutMapping("/{id}")
    public void update(@PathVariable Long id, @RequestBody ToDoRequest request){
        toDoCRUD.update(id,request);
    }
    @DeleteMapping("/{id}")
    public ToDo delete(@PathVariable Long id){
        return toDoCRUD.deleteById(id).orElseThrow(()->new ToDoNotFoundException(id));
    }
}
