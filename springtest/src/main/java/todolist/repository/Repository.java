package todolist.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import todolist.entity.ToDo;

public interface Repository extends JpaRepository<ToDo,Long> {
}
