package todolist.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class ToDoNotFoundException extends ResponseStatusException {
    public ToDoNotFoundException(Long id){
        super(HttpStatus.NOT_FOUND, "Action with id "+id+" not found");
    }
}
