package todolist.service;

import todolist.entity.ToDo;
import todolist.entity.request.ToDoRequest;

import java.util.List;
import java.util.Optional;

public interface ToDoCRUD {
    ToDo create(ToDoRequest request);

    Optional<ToDo> getById(Long id);
    void update(Long id, ToDoRequest request);
    Optional<ToDo> deleteById(Long id);
    List<ToDo> findUnfinished();
    List<ToDo> findAll();
}
