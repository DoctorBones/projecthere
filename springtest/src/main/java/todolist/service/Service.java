package todolist.service;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;
import todolist.entity.ToDo;
import todolist.entity.request.ToDoRequest;
import todolist.exception.ToDoNotFoundException;
import todolist.repository.Repository;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@org.springframework.stereotype.Service
@Transactional
public class Service implements ToDoCRUD {
    private static final Logger log= LoggerFactory.getLogger(Service.class);
    private final Repository repository;

    public Service(Repository repository){
        this.repository=repository;
    }
    @Override
    public ToDo create(ToDoRequest request){
        ToDo todo=new ToDo();
        todo.setText(request.getText());
        todo.setDone(request.getDone());
        log.info("ToDo created - "+todo);
        return repository.save(todo);
    }
    @Override
    public Optional<ToDo> getById(Long id){
        log.info("ToDo with id "+id+" was found");
        return repository.findById(id);
    }
    @Override
    public void update(Long id, ToDoRequest request){
        ToDo todo=repository.findById(id).orElseThrow(()->new ToDoNotFoundException(id));
        todo.setText(request.getText());
        todo.setDone(request.getDone());
        repository.save(todo);

    }
    @Override
    public Optional<ToDo> deleteById(Long id){
       Optional<ToDo> todo=repository.findById(id);
       todo.ifPresent(repository::delete);
       return todo;
    }
    @Override
    public List<ToDo> findUnfinished(){
        List<ToDo> todos=repository.findAll();
        List<ToDo> result= new ArrayList<>();
        for(ToDo todo:todos){
            if(!(todo.getDone())){
                result.add(todo);
            }
        }
        return result;

    }

    @Override
    public List<ToDo> findAll(){
        return repository.findAll();
    }

}
