package alevelcomnix.springtest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class GreetingsController {
    private final GreetingsService service;

    public GreetingsController(GreetingsService service) {
        this.service = service;
    }

    @GetMapping("/greeting")
    public @ResponseBody
    String greeting(){
        return service.greet();
    }
}
