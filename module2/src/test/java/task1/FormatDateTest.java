package task1;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class FormatDateTest {
    FormatDate formatDate;

    @BeforeEach
    void setUp(){
        formatDate=new FormatDate();
    }

    @Test
    public void testing(){
        List<String> list=List.of("2020/04/05","05/04/2020","04-05-2020","dfdg");
        FormatDate formatDate=new FormatDate();
        List<String> expect= List.of("20200405", "20200405", "20200405");
        assertEquals(expect,formatDate.formatedDates(list));
    }

}