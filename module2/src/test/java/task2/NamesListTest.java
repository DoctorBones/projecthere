package task2;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class NamesListTest {
    NamesList namesList;

    @BeforeEach
    void setUp(){
        namesList=new NamesList();
    }

    @Test
    public void testing(){
        String expect="Viktor";
        List<String> test=List.of("SomeName","Viktor","SomeName","SomeName1","SomeName2","SomeName1");
        assertEquals(expect,namesList.firstUniqueName(test));
    }

}