package Task3;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.LinkedList;

import static org.junit.jupiter.api.Assertions.*;

class CitiesInputTXTTest {
    CitiesInputTXT citiesInputTXT;

    @BeforeEach
    void setUp(){
        citiesInputTXT=new CitiesInputTXT();
    }

    @Test
    public void testing(){
        CitiesInputTXT input=new CitiesInputTXT();
        try {
            input.createCities("src/main/resources/routes.txt");
            var expect=new ArrayList<CityGraph.City>();
            LinkedList<CityGraph.Edge> list=new LinkedList<>();
            list.add(new CityGraph.Edge(2,1));
            list.add(new CityGraph.Edge(3,3));
            var cities=new ArrayList<CityGraph.City>();
            cities.add(new CityGraph.City("gdansk",list));
            list=new LinkedList<CityGraph.Edge>();
            list.add(new CityGraph.Edge(1,1));
            list.add(new CityGraph.Edge(3,1));
            list.add(new CityGraph.Edge(4,4));
            cities.add(new CityGraph.City("bydgoszcz",list));
            list=new LinkedList<CityGraph.Edge>();
            list.add(new CityGraph.Edge(1,3));
            list.add(new CityGraph.Edge(2,1));
            list.add(new CityGraph.Edge(4,1));
            cities.add(new CityGraph.City("torun",list));
            list=new LinkedList<CityGraph.Edge>();
            list.add(new CityGraph.Edge(2,4));
            list.add(new CityGraph.Edge(3,1));
            cities.add(new CityGraph.City("warszawa",list));
            ByteArrayOutputStream exp=new ByteArrayOutputStream();
            PrintStream streamExpect=new PrintStream(exp);
            ByteArrayOutputStream act=new ByteArrayOutputStream();
            PrintStream streamActual=new PrintStream(act);
            streamExpect.print(cities);
            streamActual.print(input.fromMapToArrayList());
            assertEquals(exp.toString(),act.toString());

        }
        catch (IOException e){
            e.printStackTrace();
        }
    }

}