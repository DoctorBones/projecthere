package Task3;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.LinkedList;

import static org.junit.jupiter.api.Assertions.*;

class CityGraphTest {
    CityGraph cityGraph;

    @Test
    public void testing(){
        LinkedList<CityGraph.Edge> list=new LinkedList<>();
        list.add(new CityGraph.Edge(2,1));
        list.add(new CityGraph.Edge(3,3));
        var cities=new ArrayList<CityGraph.City>();
        cities.add(new CityGraph.City("gdansk",list));
        list=new LinkedList<CityGraph.Edge>();
        list.add(new CityGraph.Edge(1,1));
        list.add(new CityGraph.Edge(3,1));
        list.add(new CityGraph.Edge(4,4));
        cities.add(new CityGraph.City("bydgo",list));
        list=new LinkedList<CityGraph.Edge>();
        list.add(new CityGraph.Edge(1,3));
        list.add(new CityGraph.Edge(2,1));
        list.add(new CityGraph.Edge(4,1));
        cities.add(new CityGraph.City("torun",list));
        list=new LinkedList<CityGraph.Edge>();
        list.add(new CityGraph.Edge(2,4));
        list.add(new CityGraph.Edge(3,1));
        cities.add(new CityGraph.City("Warszawa",list));
        cityGraph=new CityGraph(cities);
        assertEquals(3,cityGraph.cheapestWay(cityGraph.getCities().get(0),
                cityGraph.getCities().get(3)));
        assertEquals(2,cityGraph.cheapestWay(cityGraph.getCities().get(1),
                cityGraph.getCities().get(3)));
        assertEquals(0,cityGraph.cheapestWay(cityGraph.getCities().get(0),
                cityGraph.getCities().get(0)));
        assertEquals(1,cityGraph.cheapestWay(cityGraph.getCities().get(3),
                cityGraph.getCities().get(2)));
    }

}