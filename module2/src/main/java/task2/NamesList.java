package task2;

import java.util.*;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class NamesList {
    public String firstUniqueName(List<String> list){
        var temp=new LinkedHashSet<String>();
        var map=new HashMap<String,Integer>();
        for(int i=0, n=list.size();i<n;i++){
            if(map.containsKey(list.get(i))){
                temp.remove(list.get(i));
            }
            else{
                map.put(list.get(i),1);
                temp.add(list.get(i));
            }
        }
        return temp.iterator().next();
    }
}
