package Task3;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class CitiesInputTXT {
    private CityGraph.City [][] routsFind;
    private Map<String, CityGraph.City> cities;

    public Map<String, CityGraph.City> getCities() {
        return cities;
    }

    public CityGraph.City[][] getRoutsFind() {
        return routsFind;
    }

    public void createCities(String filename)throws IOException{
        try(BufferedReader reader=new BufferedReader(new FileReader(filename))){
            int n=Integer.parseInt(reader.readLine());
            cities= new LinkedHashMap<>();
            for(int i=0;i<n;i++){
                String name=reader.readLine();
                var list=new LinkedList<CityGraph.Edge>();
                int routesNumber=Integer.parseInt(reader.readLine());
                for(int j=0;j<routesNumber;j++){
                    String[] edge=reader.readLine().split(" ");
                    list.add(new CityGraph.Edge(Integer.parseInt(edge[0]),Integer.parseInt(edge[1])));
                }
                cities.put(name, new CityGraph.City(name,list));
            }
            int numberTofind=Integer.parseInt(reader.readLine());
            routsFind=new CityGraph.City[numberTofind][2];
            for(int i=0;i<numberTofind;i++){
                String [] route=reader.readLine().split(" ");
                routsFind[i][0]=cities.get(route[0]);
                routsFind[i][1]=cities.get(route[1]);
            }
        }
    }
    public ArrayList<CityGraph.City> fromMapToArrayList(){
        var result=new ArrayList<CityGraph.City>();
        for(String key:cities.keySet()){
            result.add(cities.get(key));
        }
        return result;
    }

    public static void main(String[] args) {
        CitiesInputTXT input=new CitiesInputTXT();
        try {
            input.createCities("module2/src/main/resources/routes.txt");
            System.out.println(input.getCities());

        }
        catch (IOException e){
            e.printStackTrace();
        }
    }
}
