package Task3;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class ShortestWayImplementation {
    public void writeWaysToFile(String filename){
        CitiesInputTXT input=new CitiesInputTXT();
        try(BufferedWriter writer=new BufferedWriter(
                new FileWriter(filename))) {
            input.createCities("module2/src/main/resources/routes.txt");
            CityGraph cityGraph=new CityGraph(input.fromMapToArrayList());
            for(int i=0, n=input.getRoutsFind().length;i<n;i++){
                int res=cityGraph.cheapestWay(input.getRoutsFind()[i][0],
                        input.getRoutsFind()[i][1]);
                writer.write(res+"\n");
            }
        }
        catch (IOException e){
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        ShortestWayImplementation shortestWayImplementation=new ShortestWayImplementation();
        shortestWayImplementation.writeWaysToFile("module2/src/main/resources/answer.txt");
    }
}
