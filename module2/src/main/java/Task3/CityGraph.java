package Task3;

import java.util.*;
import java.util.stream.Collectors;

public class CityGraph {
    static class City{
        String name;
        int bestWay;
        LinkedList<Edge> edges;
        public void setBestWay(int bestWay) {
            if(bestWay<this.bestWay) {
                this.bestWay = bestWay;
            }
        }

        public void setStartCheapesWay(){
            this.bestWay=Integer.MAX_VALUE;
        }

        public City(String name, LinkedList<Edge> edges) {
            this.name = name;
            this.edges=edges;

        }
        public String toString(){
            return name+"\n"+edges.toString()+"BW: "+bestWay;
        }
    }
    static class Edge{
        int to;
        int weight;
        public Edge(int to, int weight){
            this.to=to;
            this.weight=weight;
            }
            public String toString(){
                return to+"-"+weight;
            }

        }

    private ArrayList<City> cities;

    public CityGraph(ArrayList<City> cities) {
        this.cities = cities;
    }
    private void refresh(){
        for(int i=0, n=cities.size();i<n;i++){
            cities.get(i).setStartCheapesWay();
        }
    }


    public ArrayList<City> getCities() {
        return cities;
    }


    public int cheapestWay(City begin, City end){
        refresh();
        City temp=begin;
        temp.setBestWay(0);
        var visited=cities.stream().collect(Collectors.toCollection(HashSet::new));
        while(temp.name!=end.name){
            var list=temp.edges;
            for(int i=0, n=list.size();i<n;i++){
                City city=cities.get(list.get(i).to-1);
                if(!(visited.contains(city))){
                    continue;
                }
                city.setBestWay(temp.bestWay+list.get(i).weight);
            }
            visited.remove(temp);
            temp=visited.stream().min(Comparator.comparingInt(c->c.bestWay)).
                                            orElse(new City("Nocities",list));

        }
        return temp.bestWay;
    }

}
