package task1;

import java.time.DateTimeException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class FormatDate {
    public List<String> formatedDates(Collection<String> list){
        return list.stream().map(d-> convertDate(d)).filter(d->d!=null).
                collect(Collectors.toList());
    }
    public String convertDate(String string){
        LocalDate localDate;
        try{
            DateTimeFormatter formatter=DateTimeFormatter.ofPattern("yyyy/MM/dd");
            localDate=LocalDate.parse(string,formatter);
            return localDate.format(DateTimeFormatter.BASIC_ISO_DATE);
        }
        catch (DateTimeParseException e){

        }
        try{
            DateTimeFormatter formatter=DateTimeFormatter.ofPattern("dd/MM/yyyy");
            localDate=LocalDate.parse(string,formatter);
            return localDate.format(DateTimeFormatter.BASIC_ISO_DATE);
        }
        catch (DateTimeParseException e){

        }
        try{
            DateTimeFormatter formatter=DateTimeFormatter.ofPattern("MM-dd-yyyy");
            localDate=LocalDate.parse(string,formatter);
            return localDate.format(DateTimeFormatter.BASIC_ISO_DATE);
        }
        catch (DateTimeParseException e){

        }
        return null;
    }

    public static void main(String[] args) {
        List<String> list=List.of("2020/04/05","05/04/2020","04-05-2020","dfdg");
        FormatDate formatDate=new FormatDate();
        System.out.println(formatDate.formatedDates(list));

    }
}
