package finance;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "operation")
public class Operation {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "total_change")
    private Long totalChange;


    @ManyToOne
    @JoinColumn(name = "account_id",nullable = false)
    private Account account;



    @Column(name="category", nullable = false)
    @OneToMany(mappedBy = "operation",cascade = CascadeType.ALL, orphanRemoval = true)
    private final List<Category> categories=new ArrayList<>();

    @Embedded
    private Timestamps timestamps;

    public void addCategory(Category category){
        if(categories.size()!=0&&!(category.getClass().equals(categories.get(0).getClass()))){
            throw new IllegalArgumentException();

        }

        else {
            if(category instanceof IncomeCategory){
                if(((IncomeCategory) category).getIncome()==null){
                    throw new IllegalArgumentException();
                }
                setTotalChange(getTotalChange()+((IncomeCategory) category).getIncome());
            }
            else if(category instanceof ConsumptionCategory){
                if(((ConsumptionCategory) category).getConsumed()==null) {
                    throw new IllegalArgumentException();
                }
                setTotalChange(getTotalChange() + ((ConsumptionCategory) category).getConsumed());
            }
        }
        category.setOperation(this);
        categories.add(category);
    }
    public Timestamps getTimestamps() {
        return timestamps;
    }

    public void setTimestamps(Timestamps timestamps) {
        this.timestamps = timestamps;
    }

    public Operation() {
        this.totalChange=0L;
    }

    public Operation(Account user) {
        this.account = user;
        this.totalChange=0L;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public List<Category> getCategories() {
        return categories;
    }


    public Long getTotalChange() {
        return totalChange;
    }
    public void setTotalChange(Long totalChange){
        this.totalChange=totalChange;
    }
}
