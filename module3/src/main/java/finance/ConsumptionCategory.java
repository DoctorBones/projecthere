package finance;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "consumption_category")
public class ConsumptionCategory extends Category {
    @Column(name = "consumed", nullable = false)
    private Long consumed;

    public ConsumptionCategory() {
    }

    public ConsumptionCategory(Operation operation, Long consumed) {
        super(operation);
        if(consumed>0){
            throw new IllegalArgumentException();
        }
        this.consumed = consumed;

    }

    public Long getConsumed() {
        return consumed;
    }

    public void setConsumed(Long consumed) {
        if(consumed>0){
            throw new IllegalArgumentException();
        }

        this.consumed = consumed;

    }
}
