package finance.manipulate;

import finance.*;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class UploadTestData {


    public static void main(String[] args) {
        UploadTestData addOperation=new UploadTestData();
        Configuration configuration=new Configuration().configure();
        configuration.setProperty("hibernate.connection.username","root");
        var now= Timestamps.now();
        User user=new User();
        user.setEmail("somemail@somemail.com");
        user.setTimestamps(now);

        Account account=new Account();
        account.setUser(user);
        account.setCash(10000L);
        account.setTimestamps(now);
        user.addAccount(account);
        Operation operation=new Operation();
        operation.setAccount(account);
        operation.setTimestamps(now);
        IncomeCategory incomeCategory=new IncomeCategory();
        incomeCategory.setOperation(operation);
        incomeCategory.setIncome(100L);
        operation.addCategory(incomeCategory);
        account.addOperation(operation);
        User user1=new User();
        user1.setTimestamps(now);
        user1.setEmail("email@mail.com");
        Account account1=new Account();
        account1.setUser(user1);
        account1.setCash(5000L);
        user1.addAccount(account1);

        try(SessionFactory sessionFactory=configuration.buildSessionFactory(); Session session=sessionFactory.openSession()){
            session.beginTransaction();
            try {
                session.save(user);
                session.save(user1);
                session.getTransaction().commit();
            }
            catch (Exception e){
                session.getTransaction().rollback();
                e.printStackTrace();
            }
        }

    }
}
