package finance.manipulate;

import finance.ConsumptionCategory;
import finance.Operation;
import finance.Timestamps;

import java.time.Instant;

public class TestClass {
    public static void main(String[] args) {
        AddOperation addOperation=new AddOperation();
        Operation operation=new Operation();
        operation.setTimestamps(Timestamps.now());
        ConsumptionCategory consumptionCategory=new ConsumptionCategory();
        consumptionCategory.setConsumed(-1000L);
        operation.addCategory(consumptionCategory);
        addOperation.addOperation(1L,1L,"root","", operation);
        Instant to=Instant.now();
        Instant from=to.minusSeconds(10000000);
        Report report=new Report();
        try {
            report.printReport(1L, 1L, "root", "", from, to);
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }
}
