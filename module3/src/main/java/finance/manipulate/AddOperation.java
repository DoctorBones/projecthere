package finance.manipulate;

import finance.Account;
import finance.ConsumptionCategory;
import finance.Operation;
import finance.Timestamps;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Instant;


public class AddOperation {
    private static final Logger logger= LoggerFactory.getLogger(AddOperation.class);
    public void addOperation(Long userId, Long accId, String connectName, String connectPass, Operation op){
        Configuration configuration=new Configuration().configure();
        configuration.setProperty("hibernate.connection.username",connectName);
        configuration.setProperty("hibernate.connection.password",connectPass);
        try(SessionFactory sessionFactory=configuration.buildSessionFactory(); Session session=sessionFactory.openSession()){
            Account account=session.find(Account.class,accId);
            if(account.getUser().getId()!=userId){
                logger.info("User with id "+userId+" does not have account with id "+accId);
                return;
            }
            else{
                account.addOperation(op);
                account.getTimestamps().setModified(Instant.now());
                session.beginTransaction();
                try{
                    session.saveOrUpdate(account);
                    session.getTransaction().commit();
                    logger.info("Account was updated. Current cash: "+account.getCash()+ " Operation id "+op.getId());
                }
                catch (Exception e){
                    session.getTransaction().rollback();
                }
            }
        }
    }


}
