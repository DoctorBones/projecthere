package finance.manipulate;

import finance.Operation;
import finance.Timestamps;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.Query;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Properties;

public class Report {
    private static final Logger logger= LoggerFactory.getLogger(Report.class);
    public void printReport(Long userId, Long accId, String connectName, String connectPass, Instant from, Instant to) throws Exception{
        Properties prop =new Properties();
        try(var input = Report.class.getClassLoader().getResourceAsStream("jdbc.properties")){
            prop.load(input);
        } catch (IOException e) {
            throw new IOException();
        }
        prop.setProperty("User",connectName);
        prop.setProperty("Password",connectPass);
        String url=prop.getProperty("url");

        try(var connection = DriverManager.getConnection(url, prop);
            BufferedWriter writer=new BufferedWriter(new FileWriter("module3/report/output.csv"))) {
            writer.write("id,total_change,account_id,date_create,date_modified");
            writer.newLine();
            try (PreparedStatement query = connection.prepareStatement("select * from operation where account_id=? and begin between ? and ?")) {
                query.setLong(1,accId);
                query.setTimestamp(2,Timestamp.from(from));
                query.setTimestamp(3,Timestamp.from(to));
                var operations = query.executeQuery();
                double sum = 0;
                System.out.println(operations);
                while(operations.next()){
                    Instant insBegin=operations.getTimestamp("begin").toInstant();
                    Instant insModified=operations.getTimestamp("modified").toInstant();
                    LocalDateTime begin = LocalDateTime.ofInstant(insBegin, ZoneOffset.UTC);
                    LocalDateTime modified = LocalDateTime.ofInstant(insModified, ZoneOffset.UTC);
                    writer.write(operations.getLong("id") + "," + (double) operations.getLong("total_change") / 100 + ","
                            + operations.getLong("account_id") + "," +
                            begin + "," + modified + ",");
                    sum += (double) operations.getLong("total_change") / 100;
                    writer.newLine();
                    logger.info("Created row: "+operations.getLong("id") + "," + (double) operations.getLong("account_id") / 100 + ","
                            + operations.getLong("total_change") + "," +
                            begin + "," + modified);
                }
                writer.write("Total," + sum);
            }
        }
        catch (Exception e){
            throw e;
        }
    }

}
