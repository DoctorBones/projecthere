package finance;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.time.Instant;

@Embeddable
public class Timestamps {
    @Column(name = "begin")
    private Instant begin;

    @Column(name="modified")
    private Instant modified;

    public Timestamps() {
    }

    public Timestamps(Instant begin, Instant modified) {
        this.begin = begin;
        this.modified = modified;
    }

    public static Timestamps now() {
        var now = Instant.now();
        return new Timestamps(now, now);
    }

    public Instant getBegin() {
        return begin;
    }

    public void setBegin(Instant begin) {
        this.begin = begin;
    }

    public Instant getModified() {
        return modified;
    }

    public void setModified(Instant modified) {
        this.modified = modified;
    }
}
