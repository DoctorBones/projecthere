package finance;

import javax.persistence.*;

@Entity
@Table(name = "income_category")
public class IncomeCategory extends Category{
    @Column(name="income", nullable = false)
    private Long income;

    public Long getIncome() {
        return income;
    }

    public void setIncome(Long income) {
        if(income<=0){
            throw new IllegalArgumentException();
        }
        this.income = income;

    }

    public IncomeCategory(){

    }

    public IncomeCategory(Operation operation, Long income) throws IllegalAccessException{
        super(operation);
        if(income<=0){
            throw new IllegalArgumentException();
        }
        this.income = income;

    }

}
