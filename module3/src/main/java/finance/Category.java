package finance;

import javax.persistence.*;

@Entity(name="category")
@Inheritance(strategy=InheritanceType.JOINED)
public abstract class Category {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;


    @ManyToOne
    @JoinColumn(name = "operation_id",nullable = false)
    private Operation operation;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Operation getOperation() {
        return operation;
    }

    public void setOperation(Operation operation) {
        this.operation = operation;
    }

    public Category(Operation operation) {
        this.operation = operation;
    }

    public Category() {
    }
}
