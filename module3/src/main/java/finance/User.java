package finance;

import org.hibernate.annotations.NaturalId;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "user")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NaturalId
    @Column(name = "email", nullable = false)
    private String email;

    @Column(name = "account")
    @OneToMany(mappedBy = "user",cascade = CascadeType.ALL, orphanRemoval = true)
    private final List<Account> accounts=new ArrayList<>();

    @Embedded
    private Timestamps timestamps;

    public void addAccount(Account account){
        account.setUser(this);
        this.accounts.add(account);
    }

    public User() {
    }

    public User(String email, Timestamps timestamps) {
        this.email = email;
        this.timestamps = timestamps;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<Account> getAccounts() {
        return accounts;
    }


    public Timestamps getTimestamps() {
        return timestamps;
    }

    public void setTimestamps(Timestamps timestamps) {
        this.timestamps = timestamps;
    }
}
