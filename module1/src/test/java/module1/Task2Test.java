package module1;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Task2Test {
    Task2 task2;
    @BeforeEach
    void setUp(){
        task2=new Task2();
    }
    @Test
    public void testing(){
        assertEquals(true,task2.possibleTurn(2,2,4,1));
        assertEquals(true,task2.possibleTurn(2,2,0,1));
        assertEquals(true,task2.possibleTurn(4,0,2,1));
        assertEquals(false,task2.possibleTurn(2,2,4,4));
    }

}