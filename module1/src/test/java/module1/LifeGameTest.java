package module1;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class LifeGameTest {
    LifeGame lifeGame;
    @BeforeEach
    void setUp(){
        lifeGame=new LifeGame();
    }
    @Test
    public void testing(){
        boolean[][] test={{false,false,false,false},
                            {false,true,false,false},
                            {false,true,true,false},
                            {false,false,false,false}};
        boolean[][] expected={{false,false,false,false},
                {false,true,true,false},
                {false,true,true,false},
                {false,false,false,false}};

        assertArrayEquals(expected,lifeGame.gameStep(test));
        test=new boolean[][]{{false,false,false,false},
                {false,false,false,false},
                {false,true,true,false},
                {false,false,false,false}};
        expected=new boolean[][]{{false,false,false,false},
                {false,false,false,false},
                {false,false,false,false},
                {false,false,false,false}};
        assertArrayEquals(expected,lifeGame.gameStep(test));
        test=new boolean[][]{{false,false,false,false,false},
                {false,true,true,true,false},
                {false,true,true,false,false},
                {false,false,false,false,false}};
        expected=new boolean[][]{{false,false,false,false,false},
                {false,true,false,true,false},
                {false,true,false,true,false},
                {false,false,false,false,false}};
        assertArrayEquals(expected,lifeGame.gameStep(test));
    }

}