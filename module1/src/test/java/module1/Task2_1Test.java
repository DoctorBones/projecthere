package module1;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Task2_1Test {
    Task2_1 task2_1;

    @BeforeEach
    void setUp(){
        task2_1=new Task2_1();
    }
    @Test
    public void testing(){
        assertEquals(true, task2_1.correctString("[{()}]"));
        assertEquals(false,task2_1.correctString("][{}()"));
        assertEquals(true,task2_1.correctString("[]{}()"));
        assertEquals(true,task2_1.correctString(""));
    }
}