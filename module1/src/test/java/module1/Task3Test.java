package module1;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Task3Test {
    Task3 task3=new Task3();
    @BeforeEach
    void setUp(){
        task3=new Task3();
    }
    @Test
    public void testing(){
        int[][] test={{3,2},{7,5},{0,0}};
        assertEquals(0.5,task3.findArea(test));
        test=new int[][]{{15,15},{23,30},{50,25}};
        assertEquals(222.5,task3.findArea(test));

    }
    @Test
    public void testSpecial(){
        int[][] test={{7,5},{0,0}};
        assertEquals(0, task3.findArea(test));
    }

}