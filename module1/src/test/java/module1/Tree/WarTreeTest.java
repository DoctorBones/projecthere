package module1.Tree;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class WarTreeTest {
    WarTree warTree;
    @BeforeEach
    void setUp(){
        warTree=new WarTree();
    }
    @Test
    public void testing(){
        assertEquals(0,warTree.count(warTree.root,1));
        warTree.add(4);
        warTree.add(1);
        warTree.add(6);
        assertEquals(2,warTree.count(warTree.root,1));
        warTree.add(10);
        warTree.add(5);
        assertEquals(3,warTree.count(warTree.root,1));
        warTree.add(11);
        assertEquals(4,warTree.count(warTree.root,1));

    }

}