package module1;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Task1Test {
    Task1 task1;

    @BeforeEach
    void setUp(){
        task1=new Task1();
    }
    @Test
    public void testing(){
        int [] test={1,2,3};
        assertEquals(3,task1.findUnique(test));
        test=new int[]{1,1,2,2,3,3};
        assertEquals(3,task1.findUnique(test));
    }
    @Test
    public void testSpecial(){
        int[] test={};
        assertEquals(0,task1.findUnique(test));
        test=new int[]{1};
        assertEquals(1,task1.findUnique(test));
    }

}