package module1;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.HashMap;

public class Task2_1 {
    public boolean correctString(String target){
        if(target.trim()==""){
            return true;
        }
        if(target.length()%2!=0){
            return false;
        }
        Deque<Character> stack=new ArrayDeque<>();
        for(int i=0,n=target.length();i<n;i++){
            char temp=target.charAt(i);
            if(temp=='['||temp=='{'||temp=='('){
                stack.addLast(temp);
                continue;
            }
            else if(stack.isEmpty()){
                return false;
            }
            else if(temp==']'&&stack.getLast()=='['){
                stack.removeLast();
            }
            else if(temp=='}'&&stack.getLast()=='{'){
                stack.removeLast();
            }
            else if(temp==')'&&stack.getLast()=='('){
                stack.removeLast();
            }
            else{
                return false;
            }


        }
        return stack.isEmpty();
    }
}
