package module1;

import java.util.Arrays;

public class LifeGame {
    public boolean[][] gameStep(boolean[][] target){
        if(target.length<3||target[0].length<3){
            return target;
        }
        boolean[][] result=new boolean[target.length][target[0].length];
        for (int i = 1,n=target.length,m=target[0].length; i < n-1; i++) {
            for(int j=1;j<m-1;j++){
                int lifeCount=0;
                for(int k=i-1;k<=i+1;k++){
                    for(int l=j-1;l<=j+1;l++){
                        if(k==i&&l==j){
                            continue;
                        }
                        if(target[k][l]==true){
                            lifeCount++;
                        }
                    }
                }
                if(lifeCount>3||lifeCount<2){
                    continue;
                }
                else if(lifeCount==3){
                    result[i][j]=true;
                }
                else if(lifeCount==2){
                    result[i][j]=target[i][j];
                }

            }

        }
        return result;
    }
}
