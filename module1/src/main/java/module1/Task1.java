package module1;

import java.util.HashSet;

public class Task1 {
    public int findUnique(int[] target){
        final int n=target.length;
        if(n==0){
            return 0;
        }
        else if( n==1){
            return 1;
        }
        int count=0;
        HashSet<Integer> result=new HashSet<>();
        for (int i = 0; i < n; i++) {
            if(result.contains(target[i])){
                continue;
            }
            else{
                result.add(target[i]);
                count++;
            }
        }
        return count;
    }
}
