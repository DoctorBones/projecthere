package module1;

public class Task3 {
    public double findArea(int [][] target){
        final int n=target.length;
        if(n!=3||target[0].length!=2){
            return 0;
        }
        int[] matrix=new int[4];
        matrix[0]=target[0][0]-target[2][0];
        matrix[1]=target[0][1]-target[2][1];
        matrix[2]=target[1][0]-target[2][0];
        matrix[3]=target[1][1]-target[2][1];
        return Math.abs(0.5*((matrix[0]*matrix[3])-matrix[1]*matrix[2]));

    }
}
