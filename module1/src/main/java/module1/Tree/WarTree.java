package module1.Tree;

public class WarTree {

     Node root;

    public void add(int value) {
        root = addRecursive(root, value);
    }
    private Node addRecursive(Node current, int value) {
        if (current == null) {
            return new Node(value);
        }

        if (value < current.val) {
            current.left = addRecursive(current.left, value);
        } else if (value > current.val) {
            current.right = addRecursive(current.right, value);
        } else {

            return current;
        }

        return current;
    }
    public int count(Node warTree, int c){
        if(warTree==null){
            return 0;
        }
        int leftDepth=warTree.left==null ? c:count(warTree.left,c+1);
        int rightDepth=warTree.right==null ? c:count(warTree.right,c+1);
        return Math.max(leftDepth,rightDepth);
        }

    public void showTree(Node node){
        System.out.println(node.val);
        if(node.right!=null) showTree(node.right);
        if(node.left!=null) showTree(node.left);
    }



}
