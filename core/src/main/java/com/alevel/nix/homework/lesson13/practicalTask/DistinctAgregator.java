package com.alevel.nix.homework.lesson13.practicalTask;

import java.util.HashSet;

public class DistinctAgregator<T> implements Aggregator<Integer,T>{

    @Override
    public Integer aggregate(T[] items){
        HashSet<T> set=new HashSet<>();
        for (T item : items) {
            set.add(item);
        }
        return set.size();
    }
}
