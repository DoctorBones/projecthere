package com.alevel.nix.homework.lesson14;

public abstract class AbstractList<T> {
    abstract void remove(T t);
    abstract void add(T t);
    abstract void add(int index, T t);
    abstract T search(int i) throws Exception;
    abstract void showList();
    abstract void addFirst(T t);
    abstract void removeFirst();
}
