package com.alevel.nix.homework.lesson5;

import java.util.Arrays;

public class ZigZag {
    public String convertZigZag(String target, int numberOfRows){
        if(numberOfRows==1){
            return target;
        }
        int length=target.length();
        char[] temp= target.toCharArray();
        int row=0;
        boolean down=true;
        StringBuilder[] result=new StringBuilder[numberOfRows];
        for(int i=0;i<numberOfRows;i++){
            result[i]=new StringBuilder();
        }
        for(int i=0;i<length;i++){
            result[row].append(temp[i]);
            if(row==numberOfRows-1){
                down=false;
            }
            else if(row==0){
                down=true;
            }
            if(down){
                row++;
            }
            else{
                row--;
            }
        }
        StringBuilder finalResult=new StringBuilder();
        for(int i=0;i<result.length;i++){
            finalResult.append(result[i]);
        }
        return finalResult.toString();
    }
}
