package com.alevel.nix.homework.lesson4;

public class Trsanspone {
    public int[][] tranArr(int[][] arr){
        int firstLength=arr.length;
        int secondLength=arr[0].length;
        for (int i = 0; i < firstLength; i++) {
            if(arr[i].length!=secondLength){
                return arr;
            }
        }
        int[][] result=new int[firstLength][secondLength];
        for (int i = 0;i < firstLength; i++) {
            for(int j=0;j<secondLength;j++){
                result[j][i]=arr[i][j];
            }
        }
        return result;
    }
}
