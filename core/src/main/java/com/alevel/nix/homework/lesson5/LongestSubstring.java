package com.alevel.nix.homework.lesson5;

import java.util.HashSet;

public class LongestSubstring {
    public int findLongestSubstr(String target){
        String temp="";
        int count=0;
        for(int i=0, n=target.length(); i<n;i++){
            if(temp.contains(Character.toString(target.charAt(i)))){
                count=temp.length();
                temp=temp.substring(temp.indexOf(Character.toString(target.charAt(i)))+1);

            }
            temp+=target.charAt(i);

        }
        if(count<temp.length()){
            count=temp.length();
        }
        return count;
    }
}
