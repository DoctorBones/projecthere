package com.alevel.nix.homework.lesson13.practicalTask;


public class AvgAggregator<T extends Number> implements Aggregator<Double,T> {
    @Override
    public Double aggregate(T[] items){
        if(items.length==0){
            return null;
        }
        double sum=0;
        for (T item : items) {
            sum+=item.doubleValue();
        }
        return sum/items.length;
    }
}
