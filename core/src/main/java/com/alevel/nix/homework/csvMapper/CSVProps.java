package com.alevel.nix.homework.csvMapper;

public class CSVProps {


    @CSVParse("nameInt")
    private int someInt;

    @CSVParse("nameString")
    private String name;

    @CSVParse("nameDouble")
    double someDouble;

    @CSVParse("nameBool")
    boolean someBool;

    public String toString(){
        return "Name: " + name +" Integer: "+someInt+" Double: "+someDouble+" Boolean: "+someBool+System.lineSeparator();
    }
}
