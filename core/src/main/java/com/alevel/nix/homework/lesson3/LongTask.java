package com.alevel.nix.homework.lesson3;

import java.util.Arrays;

public class LongTask {
    public int getCount(long target){
        int count = 0;
        while (target != 0){
            count+=target & 1;
            target>>>=1;
        }
        return count;
    }




}
