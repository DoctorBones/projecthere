package com.alevel.nix.homework.lesson2;

import java.util.ArrayList;

public class Kmultiple {
    public Integer[] kmultiple(Integer[] arr, int k){
        ArrayList<Integer> res=new ArrayList<Integer>();
        for(int i=0; i< arr.length; i++){
            if(arr[i]%k==0){
                res.add(arr[i]);
            }
        }
        return res.toArray(new Integer[res.size()]);
    }
}
