package com.alevel.nix.homework.lesson12;

public interface Block<T> {
    T run() throws Exception;
}
