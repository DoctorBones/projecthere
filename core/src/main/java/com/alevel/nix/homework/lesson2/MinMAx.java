package com.alevel.nix.homework.lesson2;

public class MinMAx {
    public static int minElem(int[] arr){
        int minElem=arr[0];
        for(int i=0;i<arr.length;i++){
            if(minElem>arr[i]){
                minElem=arr[i];
            }
        }
        return minElem;
    }

    public static int maxElem(int[] arr){
        int maxElem=arr[0];
        for(int i=0;i<arr.length;i++){
            if(maxElem<arr[i]){
                maxElem=arr[i];
            }
        }
        return maxElem;
    }
}
