package com.alevel.nix.homework.threads;

import java.io.OutputStream;
import java.io.PrintStream;

import static java.lang.Thread.sleep;

public class ThreadWriter implements Runnable {
    private final PrintStream print;
    private volatile String output;

    public void setOutput(String output) {
        this.output = output;
    }

    public ThreadWriter(OutputStream out){
        print=new PrintStream(out);
        output="";
    }

    public String getOutput() {
        return output;
    }

    @Override
    public void run() {
        String prevOutput="";
        while(!(output.equals("quit"))){
                if(!(prevOutput.equals(output))) {
                    print.println(output);
                    prevOutput=output;
                }
                stop(1000);
            }
        System.out.println("finished");
    }
    public synchronized void stop(long n){
        try{
            wait(n);
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }
}
