package com.alevel.nix.homework.reflection;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.Properties;

public class AppPorpertiesInit {
    public AppProperties  loadApp() throws IOException, IllegalAccessException {
        try {
            AppProperties appProp = new AppProperties();
            Field[] fields = appProp.getClass().getDeclaredFields();
            Properties props = new Properties();
            var load = AppPorpertiesInit.class.getClassLoader().getResourceAsStream("app.properties");
            props.load(load);
            for (Field field : fields) {
                PropertyKey pk = field.getAnnotation(PropertyKey.class);
                if (pk == null) {
                    continue;
                } else if(field.getType()==String.class) {
                    field.set(appProp, props.get(pk.value()));

                }
                else{
                    field.set(appProp, Integer.parseInt((String)props.get(pk.value())));

                }

            }
            return appProp;
        }
        catch (IOException e){
            throw e;
        }
        catch (IllegalAccessException e){
            throw e;
        }

    }
}
