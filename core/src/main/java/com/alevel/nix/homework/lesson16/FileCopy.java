package com.alevel.nix.homework.lesson16;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.CopyOption;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.Arrays;
import java.util.InputMismatchException;

public class FileCopy {
    public void copyFiles(String from, String to) throws IOException {
        File target = new File(from);
        File newPlace = new File(to);
        if (target.isFile()) {
            Files.copy(target.toPath(), newPlace.toPath(),
                    StandardCopyOption.REPLACE_EXISTING);
            return;
        }
        File[] listOfFiles = target.listFiles();
        System.out.println(Arrays.toString(target.list()));
        for (File file : listOfFiles) {
            if(file.isFile()){
                File temp=new File(newPlace.getCanonicalPath()+'/'+file.getName());
                temp.createNewFile();
                Files.copy(file.toPath(),temp.toPath(),StandardCopyOption.REPLACE_EXISTING);

            }
            else if(file.isDirectory()){
                File folder = new File(to+"/"+file.getName());
                folder.mkdir();
                copyFiles(file.getCanonicalPath(),folder.getCanonicalPath());
            }


        }

    }

    public static void main(String[] args) {
        try {
            FileCopy fileCopy = new FileCopy();
            fileCopy.copyFiles("core/testFiles/fileCopyTest","core/testFiles/targetCopy");
        } catch (FileNotFoundException e) {
            System.out.println("File not found");
        } catch (IOException e) {
            System.out.println("IO exception");
            e.printStackTrace();

        }
    }
}
