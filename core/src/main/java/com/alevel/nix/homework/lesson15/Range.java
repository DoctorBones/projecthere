package com.alevel.nix.homework.lesson15;

public class Range<T extends Comparable<T>> {
    private T start;
    private T end;

    public Range(T start, T end) {
        this.start=start;
        this.end=end;
    }

    static <T extends Comparable<T>> Range<T> start(T item){
        return new Range<>(item,item);
    }

    public T getStart() {
        return start;
    }

    public T getEnd() {
        return end;
    }

    /** static <T extends Comparable<T>> Range<T> compare(T item1, T item2){
        return item1.compareTo(item2)<0 ?
                new Range<>(item1,item2) : new Range<>(item2,item1);
    }*/
   static <T extends Comparable<T>> Range<T> compareRanges(Range<T> r1, Range<T> r2){
       if(r1.start.compareTo(r2.start)<0){
           if(r1.end.compareTo(r2.end)>0){
               return r1;
           }
           return new Range<>(r1.start,r2.end);
       }
       else{
           if(r1.end.compareTo(r2.end)<0){
               return r2;
           }
           return new Range<>(r2.start, r1.end);
       }
   }


}
