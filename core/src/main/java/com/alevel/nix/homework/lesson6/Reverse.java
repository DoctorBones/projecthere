package com.alevel.nix.homework.lesson6;

public class Reverse {
    public CharSequence reverseSequence(CharSequence target){
        if(target.length()<=1){
            return target;
        }
        StringBuffer stringBuffer=new StringBuffer(target);
        CharSequence result=stringBuffer.reverse().toString();
        return result;
    }
}
