package com.alevel.nix.homework.lesson19.scaffold;

import java.io.IOException;
import java.io.PrintStream;
import java.util.Scanner;

public class RunGallowsGame {
    public void run(PrintStream printStream){
        try {
            WordsReader reader = new WordsReader("core/src/main/resources/wordsForGame.txt");
            Scanner scanner=new Scanner(System.in);
            printStream.println("Input preferred number of attempts");
            int turns=scanner.nextInt();
            GallowsGameRules game = new GallowsGameRules(reader.getChosen(),turns, printStream);
            while(game.getGameState()==GameState.CONTINUE){
                game.playerInfo();
                char attempt=scanner.next().charAt(0);
                game.checkPlayersTurn(attempt);
                game.checkGameCondition();
            }
            if(game.getGameState()==GameState.WON){
                printStream.println("Player won. Word was: "+game.getFadedWord());
            }
            else printStream.println("Player lost. Word was: "+reader.getChosen());
        }
        catch(IOException e){
            e.printStackTrace();
        }


    }

    public static void main(String[] args) {
        RunGallowsGame gallowsGame=new RunGallowsGame();
        PrintStream printStream=new PrintStream(System.out);
        gallowsGame.run(printStream);
    }
}
