package com.alevel.nix.homework.lesson11.elements;

import java.util.Scanner;

public class RunClass {
    public void run(){
        Substance substance=new Iron(20);
        System.out.println("1 for Iron, 2 for water, 3 for oxygen");
        Scanner scanner=new Scanner(System.in);
        boolean cont=false;
        while(!cont) {
            int decision = scanner.nextInt();
            switch (decision) {
                case 1:
                    substance = new Iron(20);
                    cont=true;
                    break;
                case 2:
                    substance = new Water(20);
                    cont=true;
                    break;
                case 3:
                    substance = new Oxygen(20);
                    cont=true;
                    break;
                default:
                    continue;

            }
        }

        while(true){
            System.out.println("Input temp or zero for finish: ");
            double temp = scanner.nextDouble();
            if(temp==0) break;
            System.out.println("State: "+substance.heatUp(temp)+". Temperature: " + substance.getTemp());

        }
    }

    public static void main(String[] args) {
        RunClass runClass=new RunClass();
        runClass.run();
    }
}
