package com.alevel.nix.homework.lesson11.elements;

public class Oxygen implements Substance {
    private double temp;
    private static final double MELT = -218.3;
    private static final double BOIL = -182.9;

    public Oxygen(double temp) {
        this.temp = temp;
    }
    @Override
    public double getTemp() {
        return temp;
    }

    @Override
    public State heatUp(double t){
        temp+=t;
        if(temp>=BOIL){
            return State.GAS;
        }
        else if(temp >=MELT){
            return State.LIQUID;
        }
        else return State.SOLID;
    }
}
