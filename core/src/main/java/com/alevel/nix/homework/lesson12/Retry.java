package com.alevel.nix.homework.lesson12;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Retry<T> {
    private int tries;
    private static final Logger logger= LoggerFactory.getLogger(Retry.class);
    private Block<T> block;

    public void setBlock(Block<T> block) {
        this.block = block;
    }

    public Retry(int tries, Block<T> block) {
        this.tries = tries;
        this.block=block;
    }

    public T run() throws Exception {
        int sleep=0;
        T result=null;
        for(int i=1;i<=tries;i++) {
            try {
                Thread.sleep(sleep);
                sleep+=100;
                result=block.run();
            }
            catch (Exception e){
                logger.error("Exception occured: ", e);
                if(i==tries){
                    throw e;
                }
                continue;
            }
            break;
        }
        return result;
    }


}
