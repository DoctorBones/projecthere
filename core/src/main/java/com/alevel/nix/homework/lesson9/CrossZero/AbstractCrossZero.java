package com.alevel.nix.homework.lesson9.CrossZero;

public abstract class AbstractCrossZero {
    public abstract boolean draw(int count);
    public abstract boolean canInsert();
    public abstract boolean freePlace(int index);
    public abstract void setCell(int i);
    public abstract boolean winCondition(int cell);
    public abstract void showTable();
    public abstract boolean checkDiagonal(int n);
    public abstract boolean checkReverse(int n);
    public abstract boolean checkColumn(int n);
    public abstract boolean checkRow(int n);


}
