package com.alevel.nix.homework.lesson11.elements;

public enum State {
    GAS,
    LIQUID,
    SOLID;
}
