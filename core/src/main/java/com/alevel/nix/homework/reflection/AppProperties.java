package com.alevel.nix.homework.reflection;

public class AppProperties {
    @PropertyKey("initializeInt")
    public int initializeInt;

    @PropertyKey("initializeString")
    public String initializeString;

    public int notInitialize;
}
