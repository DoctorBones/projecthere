package com.alevel.nix.homework.lesson19;

import java.time.DayOfWeek;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Weekends {
    public List<LocalDateTime> returnWeekends(LocalDateTime ld1, LocalDateTime ld2){
        return Stream.iterate(ld1,d->d.plusDays(1))
                .limit(ChronoUnit.DAYS.between(ld1,ld2)+1).
                        filter(d->d.toLocalDate().getDayOfWeek()== DayOfWeek.SATURDAY||
                                d.toLocalDate().getDayOfWeek()==DayOfWeek.SUNDAY).collect(Collectors.toList());
    }

}
