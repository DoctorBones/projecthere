package com.alevel.nix.homework.lesson9.CrossZero;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.Scanner;

public class CrossZeroGAme extends AbstractCrossZero {
    AbstractGame abstractGame;

    public CrossZeroGAme(AbstractGame abstractGame) {
        logger.debug("Start of the game");
        this.abstractGame=abstractGame;
        abstractGame.setPlayer('x');
    }

    static final Logger logger = LoggerFactory.getLogger(CrossZeroGAme.class);
    @Override
    public void setCell(int i){
        logger.debug("Player's "+abstractGame.getPlayer()+" turn");
        abstractGame.getDesk()[i]=abstractGame.getPlayer();
        if(winCondition(i)) {
            logger.debug("Player "+abstractGame.getPlayer()+" won");
        }
        else {
            if (abstractGame.getPlayer() == 'x') {
                abstractGame.setPlayer('o');
            } else {
                abstractGame.setPlayer('x');
            }
        }
    }




    @Override
    public boolean draw(int count){
        if(count==abstractGame.getDesk().length){
            logger.debug("Draw");
            return true;
        }
        return false;
    }
    @Override
    public boolean canInsert(){
        for (int i = 0; i < abstractGame.getDesk().length; i++) {
                if(abstractGame.getDesk()[i]==' '){
                    return true;
                }
            }
        return false;
    }
    @Override
    public boolean freePlace(int index){
        if(index>=abstractGame.getDesk().length||index<0){
            return false;
        }
        return abstractGame.getDesk()[index]==' ' ? true:false;
    }

    @Override
    public boolean checkRow(int cell){
        int n=(int)Math.sqrt(abstractGame.getDesk().length);
        int row=cell/n;

        for(int i=row*n;i<row*n+n-1;i++){
            if(abstractGame.getDesk()[i]!=abstractGame.getDesk()[i+1]){

                return false;
            }
        }

        return true;
    }
    @Override
    public boolean checkColumn(int cell) {
        int n = (int) Math.sqrt(abstractGame.getDesk().length);

        int column = cell % n;
        for (int i = column; i < n * n - n; i += n) {
            if (abstractGame.getDesk()[i] != abstractGame.getDesk()[i + n]) {
                return false;
            }
        }
        return true;
    }
    @Override
    public boolean checkDiagonal(int cell){
        int n = (int) Math.sqrt(abstractGame.getDesk().length);
        if(cell%(n+1)!=0){
            return false;
        }
        for(int i=0;i<n*n-(n+1);i+=n+1){
            if(abstractGame.getDesk()[i]!=abstractGame.getDesk()[i+n+1]){
                return false;
            }
        }
        return true;
    }
    @Override
    public boolean checkReverse(int cell){
        int n = (int) Math.sqrt(abstractGame.getDesk().length);
        if(cell%(n-1)!=0||cell==0){
            return false;
        }
        for(int i=n-1;i<n*n-n;i+=n-1){
            if(abstractGame.getDesk()[i]!=abstractGame.getDesk()[i+n-1]){
                System.out.println(i +" "+abstractGame.getDesk()[i]+ " "+abstractGame.getDesk()[i+n-1]);
                return false;
            }
        }
        return true;
    }
    @Override
    public boolean winCondition(int cell){
        return checkRow(cell)||checkColumn(cell)||checkDiagonal(cell)||checkReverse(cell);
    }
    @Override
    public void showTable(){
        int n = (int) Math.sqrt(abstractGame.getDesk().length);
        for(int i=0;i<n*n;i++){
            if(i%n==0){
                System.out.println(System.lineSeparator()+"------");
            }
            System.out.print(abstractGame.getDesk()[i]+"|");

        }
        System.out.println();
    }





}
