package com.alevel.nix.homework.lesson5;

public class WaterTask {
    public int biggestArea(int[] target){
        int biggestArea=0;
        for(int i=0, n=target.length-1;i<n;i++){
            for(int j=1+i;j<n+1;j++){
                if(biggestArea<Math.min(target[i],target[j])*(j-1)){
                    biggestArea=Math.min(target[i],target[j])*(j-1);
                }
            }
        }
        return biggestArea;
    }
}
