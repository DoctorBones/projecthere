package com.alevel.nix.homework.lesson19.scaffold;

public enum GameState {
    LOST,
    WON,
    CONTINUE
}
