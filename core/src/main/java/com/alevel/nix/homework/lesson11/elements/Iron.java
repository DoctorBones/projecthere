package com.alevel.nix.homework.lesson11.elements;

public class Iron implements Substance {
    private double temp;
    private static final double MELT = 1538;
    private static final double BOIL = 2861;

    public Iron(double temp) {
        this.temp = temp;
    }
    @Override
    public double getTemp() {
        return temp;
    }

    @Override
    public State heatUp(double t){
        temp+=t;
        if(temp>=BOIL){
            return State.GAS;
        }
        else if(temp >=MELT){
            return State.LIQUID;
        }
        else return State.SOLID;
    }
}
