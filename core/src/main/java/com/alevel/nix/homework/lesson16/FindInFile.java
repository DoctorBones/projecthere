package com.alevel.nix.homework.lesson16;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class FindInFile {
    public String findString(String find, String fileName) throws Exception{
        StringBuilder build=new StringBuilder();
        try(BufferedReader reader=new BufferedReader(new FileReader(fileName))){
            String temp;
            while((temp=reader.readLine())!=null){
                if(temp.contains(find)){
                    build.append(temp+ System.lineSeparator());
                }
            }
        }
        return build.toString();
    }
}
