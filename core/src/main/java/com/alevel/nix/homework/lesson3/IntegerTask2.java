package com.alevel.nix.homework.lesson3;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Arrays;

public class IntegerTask2 {
    private String finalResult;
    public void findDivisors(int target){
        if(target<0){
            target=Math.abs(target);
        }
        String[]result =new String[0];
        do{
            String [] arr =new String[result.length+1];
            arr= Arrays.copyOf(result,result.length+1);
            int temp=target%10;
            if(temp%2==0&&temp%3==0){
                arr[result.length]="fizzbuzz";
            }
            else if (temp%2==0){
                arr[result.length]="fizz";
            }
            else if(temp%3==0){
                arr[result.length]="buzz";
            }
            else{
                arr[result.length]=Integer.toString(temp);
            }
            result=Arrays.copyOf(arr, arr.length);
            target=Math.floorDiv(target,10);
        }while(target!=0);
        finalResult="";
        for(int i=result.length-1,j=0;i>=0;i--,j++){
            finalResult+=result[i]+" ";
        }
        System.out.print(finalResult+"\n");
    }



}
