package com.alevel.nix.homework.lesson3;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

public class IntegerTask1 {
    private String result;

    public void getResult(int target){
        if(target<0){
            target=Math.abs(target);
        }
        result ="";
        do{
            int temp=target%10;
            if(temp%2==0&&temp%3==0){
                result+="fizzbuzz ";
            }
            else if (temp%2==0){
                result+="fizz ";
            }
            else if(temp%3==0){
                result+="buzz ";
            }
            else{
                result+=Integer.toString(temp);
            }
            target=Math.floorDiv(target,10);
        }while(target!=0);
        System.out.print(result+"\n");

    }



}
