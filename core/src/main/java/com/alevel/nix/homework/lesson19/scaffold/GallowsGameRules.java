package com.alevel.nix.homework.lesson19.scaffold;

import java.io.PrintStream;
import java.util.ArrayList;

public class GallowsGameRules {
    private String rightWord;
    private String fadedWord;
    private GameState gameState;
    private int turnsCount;
    private PrintStream printStream;


    public void checkPlayersTurn(char c){
        if(rightWord.contains(String.valueOf(c))){
            unfadeWord(c);
        }
        else{
            turnsCount--;
        }
    }

    private void unfadeWord(char c){
        StringBuilder builder=new StringBuilder();
        for(int i=0, n=rightWord.length();i<n;i++){
            if(rightWord.charAt(i)==c){
                builder.append(c);
            }
            else if(fadedWord.charAt(i)!='*')
                builder.append(fadedWord.charAt(i));
            else builder.append('*');
            }

        fadedWord=builder.toString();
    }

    public GameState getGameState() {
        return gameState;
    }

    public void checkGameCondition(){
        if(fadedWord.contains(String.valueOf("*"))){
            if(turnsCount<1){
                gameState=GameState.LOST;
            }
            else gameState=GameState.CONTINUE;
        }
        else{
            gameState=GameState.WON;
        }
    }

    public String getFadedWord() {
        return fadedWord;
    }


    public GallowsGameRules(String rightWord, int turnsCount, PrintStream printStream) {
        this.rightWord = rightWord;
        fadedWord=new String(new char[rightWord.length()]).replace('\0','*');
        gameState=GameState.CONTINUE;
        this.turnsCount=turnsCount;
        this.printStream=printStream;
    }

    public void playerInfo(){
        printStream.println("Number of attempts: "+turnsCount);
        printStream.println("Current word state: "+fadedWord);
        printStream.println("Input letter: ");
    }

}
