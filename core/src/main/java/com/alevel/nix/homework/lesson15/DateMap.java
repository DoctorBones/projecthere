package com.alevel.nix.homework.lesson15;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.*;
import java.util.stream.Collectors;


public class DateMap {
    public Map<LocalDate, Set<LocalTime>> obtainMap(Collection<LocalDateTime> local) {
        return local.stream().collect(Collectors.groupingBy(LocalDateTime::toLocalDate,
                TreeMap::new, Collectors.mapping(LocalDateTime::toLocalTime,
                        Collectors.toCollection(TreeSet::new))));
    }
}
