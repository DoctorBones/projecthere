package com.alevel.nix.homework.threads;

import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.util.Scanner;

public class ThreadWriterDeploy {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        try(BufferedOutputStream writer=new BufferedOutputStream(new FileOutputStream("resultOfThread.txt"))){
            ThreadWriter tw=new ThreadWriter(writer);
            Thread threadWriter=new Thread(tw);

            threadWriter.start();

            while(!(tw.getOutput().equals("quit"))){
                tw.setOutput(scanner.nextLine());
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }
}
