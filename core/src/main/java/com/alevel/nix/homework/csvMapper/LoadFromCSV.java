package com.alevel.nix.homework.csvMapper;

import com.alevel.nix.homework.lesson17.CsvParser;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class LoadFromCSV {
    @FunctionalInterface
    private interface Setter{
        void set(Object target, Field field, String value) throws IllegalAccessException;
    }
    private static final Map<Class<?>,Setter> setters=Map.ofEntries(
            Map.entry(int.class,((target, field, value) -> field.set(target,Integer.parseInt(value)))),
            Map.entry(Integer.class,((target, field, value) -> field.set(target,Integer.valueOf(value)))),
            Map.entry(double.class,((target, field, value) -> field.set(target,Double.parseDouble(value)))),
            Map.entry(Double.class,((target, field, value) -> field.set(target,Double.valueOf(value)))),
            Map.entry(boolean.class,((target, field, value) -> field.set(target,Boolean.parseBoolean(value)))),
            Map.entry(Boolean.class,((target, field, value) -> field.set(target,Boolean.valueOf(value)))),
            Map.entry(String.class,((target, field, value) -> field.set(target,value)))
            );


    public ArrayList<CSVProps> load(CsvParser csvParser) throws IOException, IllegalAccessException {
        try {
            var keys=csvParser.returnKeys();
            Setter setter;

            int n=csvParser.getTable().get(keys.iterator().next()).size();
            var csvProps=new ArrayList<CSVProps>(n);
            for(int i=0;i<n;i++){
                CSVProps toAdd=new CSVProps();
                Field[] fields=toAdd.getClass().getDeclaredFields();
                for(Field field:fields) {
                    field.trySetAccessible();
                    CSVParse csvParse=field.getAnnotation(CSVParse.class);

                    if(keys.contains(csvParse.value())){
                        var valueClass=field.getType();
                        String value=csvParser.getTable().get(csvParse.value()).get(i);
                        if(valueClass.isEnum()){
                            field.set(toAdd,Enum.valueOf((Class<Enum>) valueClass,value));
                        }
                        else {
                            setters.get(valueClass).set(toAdd, field, value);
                        }
                    }
                }
                csvProps.add(toAdd);
            }
            return csvProps;
        }
        catch (IllegalAccessException e){
            throw e;
        }

    }


    public static void main(String[] args) {
        LoadFromCSV loadFromCSV=new LoadFromCSV();
        try {
            CsvParser csvParser=new CsvParser();
            csvParser.fromCsv("core/testFiles/csvFile/test.csv");
            System.out.println(loadFromCSV.load(csvParser));
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }
}
