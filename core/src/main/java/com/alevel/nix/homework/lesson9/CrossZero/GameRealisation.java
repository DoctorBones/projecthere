package com.alevel.nix.homework.lesson9.CrossZero;

import java.util.Arrays;
import java.util.Scanner;

public class GameRealisation {
    CrossZeroGAme crossZeroGAme;
    Game3x3 game3x3;
    public GameRealisation(){
        game3x3=new Game3x3();
        crossZeroGAme=new CrossZeroGAme(game3x3);
    }
    public void run(){
        Arrays.fill(game3x3.getDesk(),' ');
        int count=0;
        Scanner scanner=new Scanner(System.in);
        while(crossZeroGAme.canInsert()){
            boolean free=false;
            int index=0;
            System.out.println("Player "+game3x3.getPlayer()+ " turn");
            crossZeroGAme.showTable();
            while(!(free)){
                index=scanner.nextInt();
                free=crossZeroGAme.freePlace(index);
            }
            crossZeroGAme.setCell(index);
            count++;
            if (crossZeroGAme.winCondition(index)) {
                System.out.println("Player "+game3x3.getPlayer()+" won");
                crossZeroGAme.showTable();
                break;
            }
        }
        if(crossZeroGAme.draw(count)){
            System.out.println("It is a draw");

        }

    }

    public static void main(String[] args) {
        GameRealisation gameRealisation=new GameRealisation();
        gameRealisation.run();
    }
}
