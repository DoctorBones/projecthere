package com.alevel.nix.homework.lesson11.students;

public class Student {
    private String name;
    private int age;

    public Student(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String toString(){
        return "Name: "+name + ". Age: "+age+". ";
    }
    @Override
    public boolean equals(Object obj){
        if(this==obj){
            return true;
        }
        if(obj==null){
            return false;
        }
        Student student=(Student) obj;
        return name==student.name && age==student.age;
    }
}
