package com.alevel.nix.homework.lesson18;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class CharFrequency {

    public Map<Character,Integer> FrequencyInFile(String fileName) throws IOException{
        Map <Character,Integer> result=new HashMap<>();
        try(BufferedReader reader= new BufferedReader(new FileReader(fileName))){
            int tempInt;
            while((tempInt=reader.read())!=-1){
                Character temp=(char) tempInt;
                if(result.containsKey(temp)){
                    result.put(temp,result.get(temp)+1);
                }
                else{
                    result.put(temp,1);
                }
            }
        }

        return result;
    }

    public static void main(String[] args) {
        CharFrequency charFrequency=new CharFrequency();
        try {
            System.out.println(charFrequency.FrequencyInFile("core/testFiles/mapWithChars.txt"));
        }
        catch (IOException e){
            System.out.println("IO exception");
            e.printStackTrace();
        }
    }
}
