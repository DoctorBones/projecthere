package com.alevel.nix.homework.lesson17;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class CsvParser {
    private HashMap<String, ArrayList<String>> table;

    public HashMap<String, ArrayList<String>> getTable() {
        return table;
    }

    private ArrayList<String> indexKeys;

    public void fromCsv(String filename) throws IOException{
        table=new HashMap<>();
        indexKeys=new ArrayList<>();
        if(!(filename.matches(".*csv"))){
            throw new  IOException();
        }

        try(BufferedReader reader=new BufferedReader(new FileReader(filename))){
            String line;
            String[] tempNames=reader.readLine().split(",");
            for(int i=0, n=tempNames.length;i<n;i++){
                table.put(tempNames[i], new ArrayList<>());
                indexKeys.add(tempNames[i]);
            }
            while((line=reader.readLine())!=null){
                String[] temp= line.split(",");
                for(int i=0, n=temp.length;i<n;i++){
                    table.get(tempNames[i]).add(temp[i]);
                }
            }
        }
    }

    public String receive(int row, int column){
        if(table==null){
            return "Empty set";
        }
        return table.get(indexKeys.get(column)).get(row);
    }

    public String receive(int row, String key){
        if(table==null){
            return "Empty set";
        }
        return table.get(key).get(row);
    }
    public Set returnKeys(){
        if(table==null){
            return null;
        }
        return  table.keySet();
    }


}
