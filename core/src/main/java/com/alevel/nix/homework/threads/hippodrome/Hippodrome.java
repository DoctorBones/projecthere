package com.alevel.nix.homework.threads.hippodrome;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class Hippodrome {

    public static void main(String[] args) {
        System.out.println("Enter a number from 0 to 9");
        String guess= new Scanner(System.in).next();
        ExecutorService service = Executors.newFixedThreadPool(10);
        for (int i = 0; i < 10; i++) {
            Thread thread = new Thread(new Horse());
            service.submit(thread);
        }
        try {
            while (Horse.list.size() <= 0) {

                Thread.sleep(500);
            }
            String winner=Horse.list.get(0);
            System.out.println("Winner is: " +winner.charAt(winner.length()-1));
            service.shutdown();
        } catch (Exception e){
            e.printStackTrace();
        }
    }

}
