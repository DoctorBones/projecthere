package com.alevel.nix.homework.lesson14;

import java.util.List;

public class ForwardList<T> extends AbstractList<T> {
    private Node tail;
    private Node head;
    private int size;

    public int getSize() {
        return size;
    }

    public ForwardList() {
        size=0;
    }




    public void add(T t){
        if(tail==null){
            tail=new Node(t,0);
            head=tail;
            size++;
            return;
        }
        Node<T> append=new Node(t,size);
        tail.setNext(append);
        tail=tail.getNext();
        size++;
    }

    public void add(int index, T t) {
        if(index>size){
            add(t);
        }
        Node<T> append=new Node(t,index);
        if(head==null){
            head=new Node(t,index);
            tail=head;
            size++;
            return;
        }
        if(index==size){
            tail.setNext(append);
            tail=tail.getNext();
            size++;
            return;
        }
        else if(index==0){
            addFirst(t);
            return;
        }
        else{
            Node<T> current = head;
            boolean change=false;
            while(current.getNext()!=null){
                if(current.getNext().getIndex()==index) {
                    Node<T> swap = current.getNext();
                    current.setNext(append);
                    current.getNext().setNext(swap);
                    size++;
                    rewriteIndexes(current);
                    break;
                }
                current=current.getNext();
            }
        }

    }
    public void addFirst(T t){
        if(head==null){
            head=new Node(t,0);
            tail=head;
            size++;
            return;
        }
        Node<T> swap=head;
        head=new Node(t, 0);
        head.setNext(swap);
        rewriteIndexes(head);

    }
    public void removeFirst(){
        if(head==null){
            return;
        }
        if(size==1){
            head=null;
            return;
        }
        head=head.getNext();
        head.setIndex(0);
        rewriteIndexes(head);
    }

    public void remove(T t) {
        if (head == null) {
            return;
        }
        if(head.item==t){
            removeFirst();
            return;
        }
        Node<T> current = head;
        while (current.getNext() != null) {
            if (current.getNext().item == t) {
                current.setNext(current.getNext().getNext());
                size--;
                rewriteIndexes(current);
                break;
            }
            current=current.getNext();
        }
    }
    public T search(int index) throws IndexOutOfBoundsException{
       if(head==null){
           return null;
       }
       if(index>=size){
           throw new IndexOutOfBoundsException();
       }
        Node<T> current = head;
        while (current != null) {
            if (current.getIndex() == index) {
                break;
            }
            current=current.getNext();
        }
        return current.getItem();
    }
    public void showList(){
        if(head==null){
            System.out.println("Empty set");
        }
        Node<T> current=head;
        while(current!=null){
            System.out.println(current.item+" with index "+current.getIndex());
            current=current.getNext();
        }
    }
    private void rewriteIndexes(Node start){
        int index=start.getIndex();
        Node<T> current=start;
        while(current!=null){
            current.setIndex(index);
            index++;
            current=current.getNext();
        }
    }

    public static void main(String[] args) {
        ForwardList<Integer> list=new ForwardList<>();
        list.add(0,6);
        list.add(1,10);
        list.add(1,8);
        list.add(0,0);
        list.showList();
        list.remove(10);
        list.remove(0);
        System.out.println();
        list.showList();

    }

}
