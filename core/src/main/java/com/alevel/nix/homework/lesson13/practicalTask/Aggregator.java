package com.alevel.nix.homework.lesson13.practicalTask;

public interface Aggregator<A,T> {
    A aggregate(T[] items);
}
