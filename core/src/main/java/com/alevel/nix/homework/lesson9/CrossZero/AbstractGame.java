package com.alevel.nix.homework.lesson9.CrossZero;

public abstract class AbstractGame {
    public abstract char[] getDesk();

    public abstract void setDesk(char[] desk);
    public abstract char getPlayer();

    public abstract void setPlayer(char player);
}
