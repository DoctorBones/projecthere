package com.alevel.nix.homework.Lesson10;

public class MedianOfTwoArrays {
    public float findMedian(int[] arr1, int[] arr2){
        int n=arr1.length;
        int m=arr2.length;
        int [] res=new int[n+m];
        int i=0,j=0;
        for(int k=0;k<n+m;k++){
            if(i==n){
                res[k]=arr2[j];
                j++;
            }
            else if(j==m){
                res[k]=arr1[i];
                i++;
            }
             else if(arr1[i]<arr2[j]){
                res[k]=arr1[i];
                i++;
            }
            else{
                res[k]=arr2[j];
                j++;
            }
        }
        if((m+n)%2==0){
            return (float)(res[(m+n)/2]+res[(m+n)/2-1])/2;
        }
        else return res[(m+n)/2];

    }
}
