package com.alevel.nix.homework.lesson15;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Collection;

public class RangeImplementation {
    public Duration run(Collection<LocalDateTime> target){
        return target.stream().map(Range::start).reduce(Range::compareRanges).
                map(range->Duration.between(range.getStart(),range.getEnd())).
                orElse(Duration.ZERO);
    }
}
