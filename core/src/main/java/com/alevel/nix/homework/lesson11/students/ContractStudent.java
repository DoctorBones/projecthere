package com.alevel.nix.homework.lesson11.students;

public class ContractStudent extends Student {
    private double contract;

    public ContractStudent(String name, int age, double contract) {
        super(name, age);
        this.contract = contract;
    }

    public String toString(){
        return super.toString() + "Contract cost: "+contract;
    }
    @Override
    public boolean equals(Object obj){
        if(!super.equals(obj)){
            return false;
        }
        ContractStudent contractStudent= (ContractStudent) obj;
        return contract==contractStudent.contract;
    }
}
