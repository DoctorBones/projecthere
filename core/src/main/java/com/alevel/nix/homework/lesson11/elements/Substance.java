package com.alevel.nix.homework.lesson11.elements;

public interface Substance {
    public State heatUp(double temp);
    public double getTemp();

}
