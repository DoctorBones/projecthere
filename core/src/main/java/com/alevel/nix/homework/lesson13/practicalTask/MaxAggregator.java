package com.alevel.nix.homework.lesson13.practicalTask;

import java.util.Comparator;

public class MaxAggregator<T extends Comparable<T>> implements Aggregator<T,T> {
    @Override
    public T aggregate(T[] items){
        if(items.length==0){
            return null;
        }
        T maxT=items[0];
        for (T item : items) {
            if(maxT.compareTo(item) < 0) {
                maxT = item;
            }
        }
        return maxT;
    }
}
