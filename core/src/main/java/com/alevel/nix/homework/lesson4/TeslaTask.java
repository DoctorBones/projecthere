package com.alevel.nix.homework.lesson4;

public class TeslaTask {
    public double findBestPrice(double[] arr){
        double bestRange=arr[1]-arr[0];
        double minBuy=arr[0];
        for (int i = 0, n=arr.length-1; i < n; i++) {
            if(arr[i]<minBuy){
                minBuy=arr[i];
            }
            if(arr[i+1]-arr[i]>bestRange){
                bestRange=arr[i+1]-arr[i];
            }
            if(bestRange<arr[i+1]-minBuy){
                bestRange=arr[i+1]-minBuy;
            }
        }
        return bestRange;
    }
}
