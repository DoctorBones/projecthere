package com.alevel.nix.homework.threads.hippodrome;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.Callable;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicInteger;

public class Horse implements Runnable {
    private int distance;
    private static final Random random=new Random(100);
    static final List<String> list=new ArrayList<>();

    public Horse() {
        distance=0;
    }

    @Override
    public void run(){
        while(distance<=1000){
            distance+=random.nextInt(100)+100;
            sleepRandom();

        }
        list.add(Thread.currentThread().getName());

    }
    public void sleepRandom(){
        try{
            Thread.sleep(400+random.nextInt(100));
        }catch (InterruptedException e){
            e.printStackTrace();
        }
    }
}
