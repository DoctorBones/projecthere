package com.alevel.nix.homework.lesson14;

import java.util.List;
import java.util.stream.Collectors;

public class NumberFilter {
    public long FilterFromString(List<String> target){
        if(target.size()==0){
            return 0;
        }
        StringBuilder st=new StringBuilder();
        target.stream().collect(Collectors.joining()).chars().
                mapToObj(i->(char) i).filter(c->Character.isDigit(c)).
                forEach(c->st.append(c));
        if(st.length()==0){
            return 0;
        }
        return Long.parseLong(st.toString());
    }

}
