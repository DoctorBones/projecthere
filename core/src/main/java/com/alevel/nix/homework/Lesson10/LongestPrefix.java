package com.alevel.nix.homework.Lesson10;

public class LongestPrefix {
    public String findCommonPrefix(String[] target) {
        int n=target.length;
        if(n<1) return "";
        else if(n==1) return target[0];
        StringBuilder res = new StringBuilder();
        try {
            MainLoop:
            for(int j=0, k=target[0].length();j<k;j++){
                for (int i = 1; i < n; i++) {
                    if(target[i].charAt(j)!=target[0].charAt(j)) break MainLoop;
                }
                res.append(target[0].charAt(j));
            }
        }
        catch (StringIndexOutOfBoundsException e){
            System.out.println("Array out of bounds");
            return res.toString();
        }
        return res.toString();
    }

}
