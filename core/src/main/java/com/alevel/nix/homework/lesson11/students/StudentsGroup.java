package com.alevel.nix.homework.lesson11.students;

import java.util.Arrays;

public class StudentsGroup {
    private Student [] students;
    public StudentsGroup(){}

    public void setStudents(Student[] students) {
        this.students = students;
    }

    public StudentsGroup(Student[] students) {
        this.students = students;
    }

    public String toString(){
        StringBuilder stringBuilder=new StringBuilder();
        for(Student student: students){
            stringBuilder.append(student+"\n");
        }
        return stringBuilder.toString();
    }
    public ContractStudent [] findContract(){
        ContractStudent [] result=new ContractStudent[students.length];
        int i=0;
        for (Student student : students) {
            if(student instanceof ContractStudent){
                result[i]= (ContractStudent) student;
                i++;
            }
        }
        return Arrays.copyOf(result,i);

    }


}
