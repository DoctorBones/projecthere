package com.alevel.nix.homework.lesson19.scaffold;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Random;

public class WordsReader {
    private String chosen;
    private String[] words;

    private void readFromFile(String fileName) throws IOException {
        try(BufferedReader reader=new BufferedReader(new FileReader(fileName))){
            File file=new File(fileName);
            Path path=file.toPath();
            byte[] data = Files.readAllBytes(path);
            words=new String(data,"UTF-8").split(" ");
        }
    }
    private String chooseAtRandom(){
        final Random random=new Random();
        return words[random.nextInt(words.length)];
    }

    public WordsReader(String filename) throws IOException{
        readFromFile(filename);
        chosen=chooseAtRandom();
    }

    public String getChosen() {
        return chosen;
    }

    public String[] getWords() {
        return words;
    }
}
