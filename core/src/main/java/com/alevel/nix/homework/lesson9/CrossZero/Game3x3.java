package com.alevel.nix.homework.lesson9.CrossZero;

import java.util.Arrays;
import java.util.Scanner;

public class Game3x3 extends AbstractGame {
    private char[] desk;
    private char player;
    public Game3x3(){
        desk=new char[9];
    }

    public char[] getDesk() {
        return desk;
    }

    public void setDesk(char[] desk) {
        this.desk = desk;
    }
    public char getPlayer() {
        return player;
    }

    public void setPlayer(char player) {
        this.player = player;
    }



}
