package com.alevel.nix.homework.lesson12;


public class ArithmeticProgression {
    private int initial;
    private int step;

    public ArithmeticProgression(int initial, int step) throws ProgressionConfigurationException {
        if(step==0){
            throw new ProgressionConfigurationException(step,"Step is zero");
        }
        this.initial = initial;
        this.step = step;
    }
    public int calculate(int n) throws ProgressionConfigurationException{
        if(n<0){
            throw new ProgressionConfigurationException(n, "Number of iterations is negative: "+ n);
        }
        int a=initial;
        for(int i=0; i<n;i++){
            a+=step;
        }
        return a;
    }

    public class ProgressionConfigurationException extends Exception{
        int err;
        String errmsg;
        ProgressionConfigurationException(int err, String errmsg){
            this.err=err;
            this.errmsg=errmsg;
        }
        public void showErr(){
            System.out.println(errmsg);
        }
    }
}
