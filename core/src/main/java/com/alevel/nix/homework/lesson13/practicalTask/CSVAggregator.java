package com.alevel.nix.homework.lesson13.practicalTask;

public class CSVAggregator<T> implements Aggregator<String,T> {
    @Override
    public String aggregate(T[] items){
        StringBuilder stringBuilder=new StringBuilder();
        for (T item : items) {
            stringBuilder.append(item+",");
        }stringBuilder.deleteCharAt(stringBuilder.length()-1);
        return stringBuilder.toString();
    }
}
