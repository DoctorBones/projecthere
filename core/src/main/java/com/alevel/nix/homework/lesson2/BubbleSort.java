package com.alevel.nix.homework.lesson2;

public class BubbleSort {
    public int[] bubble(int[] res){
        for(int i=0; i< res.length-1;i++){
            for(int j=0;j<res.length-i-1;j++){
                if(res[j]>res[j+1]){
                    int temp=res[j];
                    res[j]=res[j+1];
                    res[j+1]=temp;
                }
            }
        }
        return res;
    }
}
