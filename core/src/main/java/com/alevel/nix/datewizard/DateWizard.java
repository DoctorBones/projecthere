package com.alevel.nix.datewizard;

import java.time.LocalDate;

public interface DateWizard {
    LocalDate getDateOfYear(int year, int day);
}
