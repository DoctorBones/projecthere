package com.alevel.nix.datewizard;

import java.time.LocalDate;

public class SimpleDateWizard implements DateWizard {
    public LocalDate getDateOfYear(int year, int day){
        return LocalDate.ofYearDay(year, day);
    }
}
