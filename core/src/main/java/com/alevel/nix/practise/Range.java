package com.alevel.nix.practise;

public class Range {
    public static int rangeElem(int[] arr){
        int minElem=arr[0];
        int maxElem=arr[0];
        for(int i=0;i<arr.length;i++){
            if(minElem>arr[i]){
                minElem=arr[i];
            }
            if(maxElem<arr[i]){
                maxElem=arr[i];
            }
        }
        return Math.abs(maxElem-minElem);
    }
}
