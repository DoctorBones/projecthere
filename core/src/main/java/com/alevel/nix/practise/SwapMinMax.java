package com.alevel.nix.practise;

public class SwapMinMax {
    public double[] swap(double[] arr){

        int indexOfMAx=0;
        int indexOfMin=0;
        for(int i=0;i<arr.length;i++){
            if(arr[indexOfMin]>arr[i]){
                indexOfMin=i;
            }
            if(arr[indexOfMAx]<arr[i]){
                indexOfMAx=i;
            }
        }
        double temp=arr[indexOfMAx];
        arr[indexOfMAx]=arr[indexOfMin];
        arr[indexOfMin]=temp;
        return arr;
    }
}
