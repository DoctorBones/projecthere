package com.alevel.nix.practise;

import javax.swing.*;

public class NumberOf {
    public void getAmount(double[] arr){
        int zeroCount=0, posCount=0, negCount=0;
        for (double v : arr) {
            if(v==0){
                zeroCount++;
            }
            else if(v<0){
                negCount++;
            }
            else{
                posCount++;
            }
        }
        String separator=System.lineSeparator();
        System.out.println("Number of zeros: "+zeroCount+separator+"Number of positive: "+posCount+separator+"Number of negative: "+negCount);

    }
}
