package com.alevel.nix.practise;

import java.util.Arrays;

public class ReplaceZ {
    public void replacerZ(double[] arr, double z) {
        int count=0;
        for (int i = 0, n=arr.length; i < n; i++) {
            if(arr[i]>z){
                arr[i]=z;
                count++;
            }
        }
        System.out.println("Number of replaces: "+count);
        System.out.println(Arrays.toString(arr));
    }
}

