package com.alevel.nix.practise;

import java.util.Arrays;

public class EvenNum {
    public int[] findEven(int[]arr){
        int n=arr.length;
        int k=0;
        int [] result= new int[n];
        for (int elem : arr) {
            if(elem%2==0){
                result[k]=elem;
                k++;
            }
        }
        result= Arrays.copyOf(result,k);
        if(result.length==0){
            System.out.println("No even numbers");
        }
        return result;
    }
}
