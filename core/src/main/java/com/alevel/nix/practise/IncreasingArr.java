package com.alevel.nix.practise;

public class IncreasingArr {
    public String increasingArr(double[] arr){
        for(int i=0, n=arr.length;i<n-1;i++){
            if(arr[i]>arr[i+1]){
                return "Not increasing";
            }
        }
        return "Increasing";
    }
}
