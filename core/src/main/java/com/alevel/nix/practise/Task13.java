package com.alevel.nix.practise;

public class Task13 {
    public int[] swapNeigh(int [] arr){
        for(int i=1,n=arr.length;i<n;i+=2){
            arr[i]=arr[i]^arr[i-1];
            arr[i-1]=arr[i]^arr[i-1];
            arr[i]=arr[i]^arr[i-1];
        }
        return arr;
    }
}
