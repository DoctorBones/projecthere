package com.alevel.nix.practise;

import java.util.Arrays;

public class ZeroElements {
    public int[] findZero(int [] arr){
        int n=arr.length;
        int k=0;
        int [] result= new int[n];
        for(int i=0;i<n;i++){
            if(arr[i]==0){
                result[k]=i;
                k++;
            }
        }
        result= Arrays.copyOf(result,k);
        return result;
    }


}
