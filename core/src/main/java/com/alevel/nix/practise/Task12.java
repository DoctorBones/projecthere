package com.alevel.nix.practise;

public class Task12 {
    public void findNumbers(int [] arr, int m){
        String result="";
        for(int elem:arr){
            if(elem%m<m&&elem%m>0){
                result+=elem+" ";
            }
        }
        System.out.println(result);
    }
}
