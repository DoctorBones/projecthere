package com.alevel.nix.practise;

public class WhosFirst {
    public String runBRuun(int[] arr){
        for (int i : arr) {
            if(i<0){
                return "Negative";
            }
            else if(i>0){
                return "Positive";
            }
            else{
                continue;
            }
        }
        return "All zero";
    }
}
