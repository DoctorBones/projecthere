package com.alevel.nix.practise;

public class FindFirstZero {
    public int findNumber(double[] arr){
        int count=0;
        for (double v : arr) {
            if(v==0){
                break;
            }
            count++;
        }
        return count;
    }
}
