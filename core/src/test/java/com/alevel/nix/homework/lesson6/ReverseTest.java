package com.alevel.nix.homework.lesson6;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ReverseTest {
    Reverse reverse;
    @BeforeEach
    void init(){
        reverse=new Reverse();
    }
    @Test
    public void  testing(){
        assertEquals("abcdef",reverse.reverseSequence("fedcba"));
    }
    @Test
    public void zeroTesting(){
        assertSame("",reverse.reverseSequence(""));
    }

}