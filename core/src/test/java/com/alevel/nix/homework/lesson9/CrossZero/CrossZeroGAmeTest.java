package com.alevel.nix.homework.lesson9.CrossZero;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.InputStream;
import java.io.PrintStream;

import static org.junit.jupiter.api.Assertions.*;

class CrossZeroGAmeTest {
    CrossZeroGAme crossZeroGAme;
    Game3x3 game3x3;
    @BeforeEach
    void setUp(){
        game3x3=new Game3x3();
        crossZeroGAme=new CrossZeroGAme(game3x3);
    }
    @Test
    public void testing(){
        char[] test= {'x',' ',' ',
                    ' ',' ',' ',
                    'o','o','x'};
        game3x3.setDesk(test);
        assertEquals(true,crossZeroGAme.winCondition(4));
        assertEquals(true,crossZeroGAme.canInsert());
        crossZeroGAme.setCell(2);
        assertEquals(false,crossZeroGAme.winCondition(2));
        assertEquals(true,crossZeroGAme.canInsert());
        crossZeroGAme.setCell(3);
        assertEquals(false,crossZeroGAme.winCondition(3));
        assertEquals(true,crossZeroGAme.canInsert());
        crossZeroGAme.setCell(4);
        assertEquals(true,crossZeroGAme.winCondition(4));
        assertEquals(true,crossZeroGAme.canInsert());



    }

}