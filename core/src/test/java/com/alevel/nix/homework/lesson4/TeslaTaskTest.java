package com.alevel.nix.homework.lesson4;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class TeslaTaskTest {
    TeslaTask teslaTask;
    @BeforeEach
    void initial(){
        teslaTask=new TeslaTask();
    }
    @Test
    public void testing(){
        double [] arr={5.34,10.4,12.67,3.21,21.78,22.3};
        assertEquals(19.09,teslaTask.findBestPrice(arr));
        arr=new double[]{56.66,43.7,35.78,22.7,100.3,3.9};
        assertEquals(77.6,teslaTask.findBestPrice(arr));
        arr=new double[]{34.6,10.5,56.1,32.7,77.7};
        assertEquals(67.2,teslaTask.findBestPrice(arr));
    }
}