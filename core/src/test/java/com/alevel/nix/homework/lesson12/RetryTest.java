package com.alevel.nix.homework.lesson12;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RetryTest {
    @Test
    public void testingInt(){

            Block<Integer> block = () -> {Integer a=2/0; return a;};
            Retry<Integer> retry = new Retry<>(8, block);
            assertThrows(Exception.class, ()->retry.run());
            block=()-> {return 3+8;};
            retry.setBlock(block);
            try {
                assertEquals(11, retry.run());
            }
            catch (Exception e){
                e.printStackTrace();
            }
    }
    @Test
    public void testingString(){
        Block<String> block=()->{StringBuilder test=null; return test.toString();};
        Retry<String> retry=new Retry<>(5,block);
        assertThrows(Exception.class, ()->retry.run());
        block=()-> {return "Hello world";};
        retry.setBlock(block);
        try {
            assertEquals("Hello world", retry.run());
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

}