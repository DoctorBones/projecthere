package com.alevel.nix.homework.lesson14;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class NumberFilterTest {
    NumberFilter numberFilter;

    @BeforeEach
    void setUp(){
        numberFilter=new NumberFilter();
    }

    @Test
    public void testing(){
        List<String> list=List.of("12","Hello6","fdssdf34dsfsf");
        assertEquals(12634,numberFilter.FilterFromString(list));
        list=List.of("1","2","3");
        assertEquals(123,numberFilter.FilterFromString(list));
    }

    @Test
    public void testingEmpty(){
        List<String> list=List.of("","Hello","fdssdfdsfsf");
        assertEquals(0,numberFilter.FilterFromString(list));
        list=List.of();
        assertEquals(0,numberFilter.FilterFromString(list));
    }
}