package com.alevel.nix.homework.lesson5;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class WaterTaskTest {
    WaterTask waterTask=new WaterTask();
    @BeforeEach
    void initial(){
        waterTask=new WaterTask();
    }
    @Test
    public void testing(){
        int[] test={1,8,6,2,5,4,8,3,7};
        assertEquals(49, waterTask.biggestArea(test));
        test=new int[]{1};
        assertEquals(0, waterTask.biggestArea(test));

    }

}