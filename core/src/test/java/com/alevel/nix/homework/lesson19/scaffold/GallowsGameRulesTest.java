package com.alevel.nix.homework.lesson19.scaffold;

import org.junit.jupiter.api.Test;

import java.io.PrintStream;

import static org.junit.jupiter.api.Assertions.*;
class GallowsGameRulesTest {

    GallowsGameRules rules;

    @Test
    public void testing(){
        PrintStream printStream=new PrintStream(System.out);
        rules=new GallowsGameRules("table",4,printStream);
        assertEquals("*****",rules.getFadedWord());
        assertEquals(GameState.CONTINUE, rules.getGameState());
        rules.checkPlayersTurn('a');
        rules.checkGameCondition();
        assertEquals("*a***",rules.getFadedWord());
        assertEquals(GameState.CONTINUE, rules.getGameState());
        rules.checkPlayersTurn('e');
        rules.checkPlayersTurn('l');
        assertEquals("*a*le",rules.getFadedWord());
        assertEquals(GameState.CONTINUE, rules.getGameState());
        rules.checkPlayersTurn('t');
        rules.checkPlayersTurn('b');
        rules.checkGameCondition();
        assertEquals("table",rules.getFadedWord());
        assertEquals(GameState.WON, rules.getGameState());

    }
    @Test
    public void failourTest(){
        PrintStream printStream=new PrintStream(System.out);
        rules=new GallowsGameRules("table",4,printStream);
        rules.checkPlayersTurn('m');
        rules.checkGameCondition();
        assertEquals("*****",rules.getFadedWord());
        assertEquals(GameState.CONTINUE, rules.getGameState());
        rules.checkPlayersTurn('n');
        rules.checkPlayersTurn('t');
        assertEquals("t****",rules.getFadedWord());
        assertEquals(GameState.CONTINUE, rules.getGameState());
        rules.checkPlayersTurn('o');
        rules.checkPlayersTurn('q');
        rules.checkGameCondition();
        assertEquals("t****",rules.getFadedWord());
        assertEquals(GameState.LOST, rules.getGameState());


    }

}