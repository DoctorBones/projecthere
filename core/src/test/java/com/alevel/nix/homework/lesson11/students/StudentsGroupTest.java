package com.alevel.nix.homework.lesson11.students;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class StudentsGroupTest {
    StudentsGroup studentsGroup;
    @BeforeEach
    void setUp(){
        studentsGroup=new StudentsGroup();
    }

    @Test
    public void testing() {
        Student[] students = {new Student("SomeName1", 19),
                new Student("SomeName2", 20),
                new ContractStudent("SomeName3", 19, 1500),
                new ContractStudent("SomeName4", 21, 1500)};
        studentsGroup.setStudents(students);
        System.out.println(studentsGroup.toString());
        ContractStudent[] test= {new ContractStudent("SomeName3", 19, 1500),
        new ContractStudent("SomeName4", 21, 1500)};
        assertArrayEquals(test,studentsGroup.findContract());
        students = new Student[]{new Student("SomeName1", 19),
                new ContractStudent("SomeName2", 20,1700),
                new ContractStudent("SomeName3", 19, 1500),
                new ContractStudent("SomeName4", 21, 1500)};
        test=new ContractStudent[]{new ContractStudent("SomeName2", 20,1700),
                new ContractStudent("SomeName3", 19, 1500),
                new ContractStudent("SomeName4", 21, 1500)};
        studentsGroup.setStudents(students);
        assertArrayEquals(test,studentsGroup.findContract());

        }

    }

