package com.alevel.nix.homework.lesson2;

import com.alevel.nix.homework.lesson2.BubbleSort;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BubbleSortTest {
    private BubbleSort bubbleSort;
    @BeforeEach
    void initial(){
        bubbleSort=new BubbleSort();
    }
    @Test
    void testEver(){
        int[] test={3,6,7,1,2};
        int[] expected={1,2,3,6,7};
        assertArrayEquals(expected,bubbleSort.bubble(test));
        test=new int[]{65,32,12,9,65,323};
        expected=new int[]{9,12,32,65,65,323};
        assertArrayEquals(expected,bubbleSort.bubble(test));
        test=new int[]{43,12,56,78,42,88,99,100,1};
        expected=new int[]{1,12,42,43,56,78,88,99,100};
        assertArrayEquals(expected,bubbleSort.bubble(test));
    }
}