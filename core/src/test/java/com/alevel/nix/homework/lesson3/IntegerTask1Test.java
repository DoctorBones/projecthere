package com.alevel.nix.homework.lesson3;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

class IntegerTask1Test {
    IntegerTask1 integerTask1;
    @BeforeEach
    void initial(){
        integerTask1=new IntegerTask1();
    }
    @Test
    public void testing(){

        ByteArrayOutputStream cage=new ByteArrayOutputStream();
        PrintStream f=new PrintStream(cage);
        System.setOut(f);
        integerTask1.getResult(236);
        integerTask1.getResult(0);
        integerTask1.getResult(-2222);
        String expected = "fizzbuzz buzz fizz \n";
        expected+="fizzbuzz \n";
        expected+="fizz fizz fizz fizz \n";
        assertEquals(expected, cage.toString());

    }
}