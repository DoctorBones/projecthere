package com.alevel.nix.homework.lesson19;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class WeekendsTest {
    Weekends weekends;

    @BeforeEach
    void setUp(){
        weekends=new Weekends();
    }
    @Test
    public void testing(){
        LocalDateTime now=LocalDateTime.now();
        List<LocalDateTime> expect=List.of(now,now.plusDays(1),now.plusDays(7));
        assertEquals(expect,weekends.returnWeekends(now.minusDays(4),now.plusDays(7)));

    }

}