package com.alevel.nix.homework.lesson3;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.jupiter.api.Assertions.*;

class IntegerTask2Test {
    IntegerTask2 integerTask2;
    @BeforeEach
    void initial(){
        integerTask2=new IntegerTask2();
    }
    @Test
    public void testing(){
        ByteArrayOutputStream cage=new ByteArrayOutputStream();
        PrintStream f=new PrintStream(cage);
        System.setOut(f);
        integerTask2.findDivisors(236);
        integerTask2.findDivisors(0);
        integerTask2.findDivisors(-2233);
        String expected = "fizz buzz fizzbuzz \n";
        expected+="fizzbuzz \n";
        expected+="fizz fizz buzz buzz \n";
        assertEquals(expected, cage.toString());
    }

}