package com.alevel.nix.homework.lesson2;

import com.alevel.nix.homework.lesson2.MinMAx;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MinMAxTest {
    private MinMAx minMAx;
    @BeforeEach
    void initial(){
       minMAx=new MinMAx();
    }
    private void assertMin(int[] arr, int expected){
        assertEquals(expected, MinMAx.minElem(arr));
    }
    private void assertMax(int[] arr, int expected){
        assertEquals(expected, MinMAx.maxElem(arr));
    }
    @Test
    void testAll(){
        int[] res={3,6,7,1,2};
        assertMin(res, 1);
        assertMax(res,7);
        res=new int[]{65,32,12,9,65,323};
        assertMin(res,9);
        assertMax(res,323);
        res=new int[]{43,12,56,78,42,88,99,100,1};
        assertMin(res, 1);
        assertMax(res,100);

    }
}