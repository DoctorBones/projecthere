package com.alevel.nix.homework.lesson11.elements;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SubstanceTest {
    Substance substance;

    @Test
    public void testing(){
        substance=new Iron(20);
        assertEquals(State.SOLID, substance.heatUp(20));
        assertEquals(State.LIQUID, substance.heatUp(1500));
        assertEquals(State.GAS, substance.heatUp(1500));
        substance=new Water(20);
        assertEquals(State.SOLID, substance.heatUp(-40));
        assertEquals(State.GAS, substance.heatUp(1500));
        substance=new Oxygen(20);
        assertEquals(State.LIQUID, substance.heatUp(-220));
        assertEquals(State.SOLID, substance.heatUp(-400));
    }

}