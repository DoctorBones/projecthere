package com.alevel.nix.homework.reflection;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Field;

import static org.junit.jupiter.api.Assertions.*;

class AppPorpertiesInitTest {
    AppPorpertiesInit appPorpertiesInit;

    @BeforeEach
    void setUp(){
        appPorpertiesInit=new AppPorpertiesInit();
    }

    @Test
    public void testing(){
        try {
            var app = appPorpertiesInit.loadApp();
            assertEquals(64,app.initializeInt);
            assertEquals("Reflection is power",app.initializeString);
            assertEquals(0,app.notInitialize);
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }
}