package com.alevel.nix.homework.lesson16;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.FileNotFoundException;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

class FindInFileTest {
    FindInFile findInFile;

    @BeforeEach
    void setUp(){
        findInFile=new FindInFile();
    }

    @Test
    public void testing(){
        String separator= System.lineSeparator();
        String test ="with hello"+separator+"hello again"+separator;
        try {
            assertEquals(test, findInFile.findString("hello",
                    "testFiles/findInFile.txt"));
        }
        catch (FileNotFoundException e){
            System.out.println("File not found");
        }
        catch (IOException e){
            System.out.println("Invalid input");
        }
        catch (Exception e){
            System.out.println("Unknown exception");
        }

    }
    @Test
    public void exceptionsTesting(){
        assertThrows(FileNotFoundException.class,()->findInFile.findString("somestring",
                "someFile"));
    }
}