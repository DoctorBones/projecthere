package com.alevel.nix.homework.Lesson10;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MedianOfTwoArraysTest {
    MedianOfTwoArrays medianOfTwoArrays;
    @BeforeEach
    void setUp(){
        medianOfTwoArrays=new MedianOfTwoArrays();

    }
    @Test
    public void testing(){
        int[] arr1={1,3};
        int[] arr2={2};
        assertEquals(2,medianOfTwoArrays.findMedian(arr1,arr2));
        arr1=new int[]{1,2};
        arr2=new int[]{3,4};
        assertEquals(2.5,medianOfTwoArrays.findMedian(arr1,arr2));
    }

}