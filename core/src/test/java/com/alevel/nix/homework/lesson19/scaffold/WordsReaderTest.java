package com.alevel.nix.homework.lesson19.scaffold;

import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

class WordsReaderTest {
    WordsReader reader;

    @Test
    public void testing(){
        try {
            reader = new WordsReader("src/main/resources/wordsForGame.txt");
            String[] test={"hello", "table", "door", "fear", "kitchen", "dog", "cat", "sheep"};
            System.out.println(Arrays.toString(reader.getWords()));
            assertArrayEquals(test, reader.getWords());
            System.out.println(reader.getChosen());

        }
        catch (IOException e){
            e.printStackTrace();
        }
    }

}