package com.alevel.nix.homework.Lesson10;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class LongestPrefixTest {
    LongestPrefix longestPrefix;
    @BeforeEach
    void setUp(){
        longestPrefix=new LongestPrefix();
    }
    @Test
    public void testing(){
        String [] test={"dogBig","dogSmall","dogAverage"};
        assertEquals("dog",longestPrefix.findCommonPrefix(test));
        test=new String[]{"dogBig","dogSmall","AverageDog"};
        assertEquals("",longestPrefix.findCommonPrefix(test));
        test=new String[]{"dogBig","dogSmall"};
        assertEquals("dog",longestPrefix.findCommonPrefix(test));
        test=new String[]{"dogBig"};
        assertEquals("dogBig",longestPrefix.findCommonPrefix(test));
        test=new String[]{"dogBig","dog"};
        assertEquals("dog",longestPrefix.findCommonPrefix(test));

    }

}