package com.alevel.nix.homework.lesson12;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ArithmeticProgressionTest {

    @Test
    public void testing(){
        try {
            ArithmeticProgression arithmeticProgression = new ArithmeticProgression(9, 0);
        }
        catch (ArithmeticProgression.ProgressionConfigurationException e){
            e.showErr();
        }
        try{
            ArithmeticProgression arithmeticProgression=new ArithmeticProgression(9,90);
            assertThrows(ArithmeticProgression.ProgressionConfigurationException.class,
                    ()-> arithmeticProgression.calculate(-10));
        }
        catch (ArithmeticProgression.ProgressionConfigurationException e){
            e.showErr();
        }
        try{
            ArithmeticProgression arithmeticProgression=new ArithmeticProgression(9,90);

            assertEquals(909, arithmeticProgression.calculate(10));
        }
        catch (ArithmeticProgression.ProgressionConfigurationException e){
            e.showErr();
        }
    }

}