package com.alevel.nix.homework.lesson5;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class LongestSubstringTest {
    LongestSubstring longestSubstring;
    @BeforeEach
    void initial(){
        longestSubstring=new LongestSubstring();
    }
    @Test
    public void testing(){
        assertEquals(5, longestSubstring.findLongestSubstr("abacde"));
        assertEquals(1, longestSubstring.findLongestSubstr("aaaaaaa"));
        assertEquals(3, longestSubstring.findLongestSubstr("hello"));
        assertEquals(0, longestSubstring.findLongestSubstr(""));
    }
}