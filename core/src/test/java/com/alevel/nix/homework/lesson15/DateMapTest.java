package com.alevel.nix.homework.lesson15;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class DateMapTest {
    DateMap dateMap;

    @BeforeEach
    void setUp(){
        dateMap=new DateMap();
    }

    @Test
    public void testing(){
        LocalDateTime now=LocalDateTime.now();
        List<LocalDateTime> list=List.of(now,now.minusDays(2),now.plusHours(10),
            now.minusMinutes(20),now.plusDays(1), now.minusDays(3));
        System.out.println(dateMap.obtainMap(list));
        Set<LocalTime> set1=Set.of(now.minusDays(3).toLocalTime());
        Set<LocalTime> set2=Set.of(now.minusDays(2).toLocalTime());
        Set<LocalTime> set3=Set.of(now.toLocalTime(),now.minusMinutes(20).toLocalTime());
        Set<LocalTime> set4=Set.of(now.plusDays(1).toLocalTime(),now.plusHours(10).toLocalTime());
        Map<LocalDate,Set<LocalTime>> test=Map.of(now.minusDays(3).toLocalDate(),set1,
               now.minusDays(2).toLocalDate(),set2,now.toLocalDate(),set3,
                now.plusDays(1).toLocalDate(),set4);
        assertEquals(test,dateMap.obtainMap(list));
    }
}