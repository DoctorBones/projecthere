package com.alevel.nix.homework.lesson13.practicalTask;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MaxAggregatorTest {
    MaxAggregator agregator;

    @Test
    public void testingInt(){
        Integer[] test={1,2,3,4,5,6};
        agregator=new MaxAggregator();
        assertEquals(6,agregator.aggregate(test));
    }
    @Test
    public void testingDouble(){
        Double[] test={1.00,2.00,3.00,10.00,4.00,5.00,6.00};
        agregator=new MaxAggregator();
        assertEquals(10.00,agregator.aggregate(test));
    }
    @Test
    public void testingNull(){
        Double[] test={};
        agregator=new MaxAggregator();
        assertNull(agregator.aggregate(test));
    }
    @Test
    void testingString(){
        agregator=new MaxAggregator();
        String[] test={"Dog","BigDog","SmallDog","Dog"};
        assertEquals("SmallDog",agregator.aggregate(test));

    }
}