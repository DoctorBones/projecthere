package com.alevel.nix.homework.lesson14;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ForwardListTest {
    @Test
    public void testingInt(){
        ForwardList<Integer>list=new ForwardList<>();
        list.add(10);
        list.add(39);
        assertEquals(2,list.getSize());
        assertEquals(10, list.search(0));
        assertEquals(39, list.search(1));
        list.remove(10);
        assertEquals(39,list.search(0));
        list.add(0, 100);
        assertEquals(100,list.search(0));
        list.add(10);
        list.add(1,20);
        assertEquals(20,list.search(1));
        assertThrows(IndexOutOfBoundsException.class, ()->list.search(10));


    }
    @Test
    public void testingString(){
        ForwardList<String>list=new ForwardList<>();
        list.add("Dog");
        list.add("BigDog");
        assertEquals(2,list.getSize());
        assertEquals("Dog", list.search(0));
        assertEquals("BigDog", list.search(1));
        list.remove("BigDog");
        assertEquals(1,list.getSize());

    }

}