package com.alevel.nix.homework.lesson16;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.*;

import static org.junit.jupiter.api.Assertions.*;

class FileCopyTest {
    FileCopy fileCopy;

    @BeforeEach
    void setUp() {
        fileCopy = new FileCopy();
    }

    @Test
    public void testing() {
        File file = new File("testFiles/targetCopy/folder1/folder3/10.txt");
        file.delete();
        assertEquals(file.exists(), false);
        try {
         FileCopy fileCopy = new FileCopy();
         fileCopy.copyFiles("testFiles/fileCopyTest","testFiles/targetCopy");
         } catch (FileNotFoundException e) {
            System.out.println("File not found");
        } catch (IOException e) {
            System.out.println("IO exception first part");
            e.printStackTrace();
        }
         assertEquals(true,file.exists());
        try(BufferedReader reader=new BufferedReader(new FileReader(file))){
            assertEquals("lkksjsljdknsfd",reader.readLine());
        }
        catch (IOException e){
            System.out.println("IO exception second part");
        }

    }

}
