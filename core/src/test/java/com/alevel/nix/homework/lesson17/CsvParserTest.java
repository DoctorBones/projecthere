package com.alevel.nix.homework.lesson17;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class CsvParserTest {
    CsvParser csv;

    @BeforeEach
    void setUP(){
        csv=new CsvParser();
    }

    @Test
    public void testin(){
        try {
            csv.fromCsv("testFiles/csvFile/test.csv");
            assertEquals("obj1",csv.receive(0,0));
            assertEquals("obj1",csv.receive(0,"name1"));
            assertEquals("obj11",csv.receive(2,2));
            assertEquals("obj11",csv.receive(2,"name3"));
            Set<String> test=Set.of("name1","name2","name3","name4");
            assertEquals(test,csv.returnKeys());
        }
        catch (FileNotFoundException e){
            System.out.println("A file to gde, M?");
            e.printStackTrace();
        }
        catch (IOException e){
            System.out.println("Input is wounded");
            e.printStackTrace();
        }
    }
    @Test
    public void testExcpetrion(){
        assertThrows(IOException.class,()->csv.fromCsv("testFiles/csvFile/test.txt"));
    }

}