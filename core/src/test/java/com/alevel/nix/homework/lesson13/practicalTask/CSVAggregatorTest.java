package com.alevel.nix.homework.lesson13.practicalTask;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CSVAggregatorTest {
    CSVAggregator aggregator;

    @Test
    void testingString(){
        aggregator=new CSVAggregator<String>();
        String[] test={"Dog","BigDog","SmallDog","Dog"};
        assertEquals("Dog,BigDog,SmallDog,Dog",aggregator.aggregate(test));

    }
    @Test
    void testingInt(){
        aggregator=new CSVAggregator();
        Integer[] test={1,2,3,4,1,2,3,4};
        assertEquals("1,2,3,4,1,2,3,4",aggregator.aggregate(test));
    }

}