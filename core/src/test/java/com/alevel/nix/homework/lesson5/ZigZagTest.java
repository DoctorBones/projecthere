package com.alevel.nix.homework.lesson5;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ZigZagTest {
    ZigZag zigZag=new ZigZag();
    @BeforeEach
    void initial(){
        zigZag=new ZigZag();
    }
    @Test
    public void testing(){
        assertEquals("ACEGBDFH",zigZag.convertZigZag("ABCDEFGH",2));
        assertEquals("PAHNAPLSIIGYIR",zigZag.convertZigZag("PAYPALISHIRING",3));
    }
    @Test
    public void testNull(){
        String test="dsfdffs";
        assertSame(test, zigZag.convertZigZag(test,1));
    }
}