package com.alevel.nix.homework.lesson3;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class LongTaskTest {
    LongTask longTask;
    @BeforeEach
    void intitilal(){
        longTask=new LongTask();
    }
    @Test
    public void testing(){
        assertEquals(2,longTask.getCount(10));
        assertEquals(7,longTask.getCount(1000000));
        assertEquals(1,longTask.getCount(1));
        assertEquals(60,longTask.getCount(-100));
    }

}