package com.alevel.nix.homework.lesson13.practicalTask;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DistinctAgregatorTest {
    DistinctAgregator agregator;
    @Test
    void testingString(){
        agregator=new DistinctAgregator<String>();
        String[] test={"Dog","BigDog","SmallDog","Dog"};
        assertEquals(3,agregator.aggregate(test));

    }
    @Test
    void testingInt(){
        agregator=new DistinctAgregator<Integer>();
        Integer[] test={1,2,3,4,1,2,3,4};
        assertEquals(4,agregator.aggregate(test));
    }

}