package com.alevel.nix.homework.lesson13.practicalTask;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class AvgAggregatorTest {
   AvgAggregator agregator;

    @Test
    public void testingInt(){
        Integer[] test={1,2,3,4,5,6};
        agregator=new AvgAggregator<Integer>();
        assertEquals(3.5,agregator.aggregate(test));
    }
    @Test
    public void testingDouble(){
        Double[] test={1.00,2.00,3.00,4.00,5.00,6.00};
        agregator=new AvgAggregator<Integer>();
        assertEquals(3.5,agregator.aggregate(test));
    }
    @Test
    public void testingNull(){
        Double[] test={};
        agregator=new AvgAggregator<Integer>();
        assertNull(agregator.aggregate(test));
    }

}