package com.alevel.nix.homework.lesson15;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class RangeImplementationTest {
    private RangeImplementation range;

    @BeforeEach
    void setUp(){
        range=new RangeImplementation();
    }

    @Test
    public void testing(){
        LocalDateTime now=LocalDateTime.now();
        List<LocalDateTime> list=List.of(now,now.minusDays(2),now.plusHours(10),
                now.minusMinutes(20),now.plusDays(1), now.minusDays(3));
        Duration test=Duration.between(now.minusDays(3), now.plusDays(1));
        assertEquals(test.toDays(),range.run(list).toDays());
        list=List.of(now, now.plusMinutes(20), now.minusMinutes(20));
        test=Duration.between(now.minusMinutes(20),now.plusMinutes(20));
        assertEquals(test.toMinutes(),range.run(list).toMinutes());
    }

}