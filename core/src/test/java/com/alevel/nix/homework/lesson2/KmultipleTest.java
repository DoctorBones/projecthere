package com.alevel.nix.homework.lesson2;

import com.alevel.nix.homework.lesson2.Kmultiple;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class KmultipleTest {
    private Kmultiple kmul;
    @BeforeEach
    void initial(){
        kmul=new Kmultiple();
    }
    @Test
    public void testing(){
        Integer[] test={3,6,7,1,2};
        Integer[] expected={6,2};
        assertArrayEquals(expected, kmul.kmultiple(test,2));
        test=new Integer[]{65,32,12,9,65,323};
        expected = new Integer[]{12,9};
        assertArrayEquals(expected, kmul.kmultiple(test,3));
        expected = new Integer[]{65,65};
        assertArrayEquals(expected, kmul.kmultiple(test,5));

    }

}