package com.alevel.nix.practise;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

class ZeroElementsTest {
    public static void main(String[] args) {
        ZeroElements zeroElements=new ZeroElements();
        int[] test={3,2,0,0,9,0,3,2};
        System.out.println(Arrays.toString(zeroElements.findZero(test)));
        WhosFirst whosFirst=new WhosFirst();
        System.out.println(whosFirst.runBRuun(test));
        IncreasingArr increasingArr=new IncreasingArr();
        double[] doubleTest={-1.1,3.5,12.8,45.6};
        System.out.println(increasingArr.increasingArr(doubleTest));
        test=new int[]{1,2,3,4,5,6,10};
        EvenNum evenNum=new EvenNum();
        System.out.println(Arrays.toString(evenNum.findEven(test)));
        test=new int[]{4,7,8,32,46,-10};
        System.out.println(Range.rangeElem(test));
        ReplaceZ replaceZ=new ReplaceZ();
        replaceZ.replacerZ(doubleTest,7.7);
        FindFirstZero findFirstZero=new FindFirstZero();
        doubleTest=new double[]{9.9,4.5,-12.6,0,1.1};
        System.out.println(findFirstZero.findNumber(doubleTest));
        NumberOf numberOf=new NumberOf();
        numberOf.getAmount(doubleTest);
        SwapMinMax swapMinMax=new SwapMinMax();
        System.out.println(Arrays.toString(swapMinMax.swap(doubleTest)));
        FindNumbers findNumbers=new FindNumbers();
        test=new int[]{1,1,0,78,6,4,5};
        findNumbers.prinNumbers(test);
        Task12 task12=new Task12();
        task12.findNumbers(test,6);
        Task13 task13=new Task13();
        System.out.println(Arrays.toString(task13.swapNeigh(test)));
    }

}